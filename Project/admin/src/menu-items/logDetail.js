import { IconHistory} from "@tabler/icons";


const icons= {IconHistory};

const logDetail =  {
    id: 'log',
    title: 'Log Stok',
    type: 'group',
    children : [
        {
            id : 'log',
            title: 'Log Stok',
            type: 'item',
            url: '/log/detail',
            icon: icons.IconHistory,
            breadcrumbs: false
        }
    ]
}
 
export default logDetail;