import { IconHistory} from "@tabler/icons";


const icons= {IconHistory};

const logStock =  {
    id: 'log',
    title: 'Log Stok',
    type: 'group',
    children : [
        {
            id : 'log',
            title: 'Log Stok',
            type: 'item',
            url: '/log',
            icon: icons.IconHistory,
            breadcrumbs: false
        }
    ]
}
 
export default logStock;