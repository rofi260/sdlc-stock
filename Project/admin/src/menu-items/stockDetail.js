import { IconBuildingWarehouse } from "@tabler/icons";

const icons = {IconBuildingWarehouse};
const StockDetail =  {
    id: 'monitoringStock',
    title: 'Monitoring Stok',
    type: 'group',
    children: [
        {
            id: 'monitoringStock',
            title: 'Monitoring Stok',
            type: 'item',
            url: '/stok/detail/:id',
            icon: icons.IconBuildingWarehouse,
            breadcrumbs: false
        }
    ]
}
 
export default StockDetail;