// assets
import { IconDatabase } from '@tabler/icons';

// constant
const icons = { IconDatabase };

const productDetail = {
    id: 'masterProduct',
    title: 'Master Produk',
    type: 'group',
    children: [
        {
            id: 'masterProduct',
            title: 'Master Produk',
            type: 'item',
            url: '/product/detail',
            icon: icons.IconDatabase,
            breadcrumbs: false
        }
    ]
};

export default productDetail;