import { IconBuildingWarehouse } from "@tabler/icons";

const icons = {IconBuildingWarehouse};
const monitoringStock =  {
    id: 'stok',
    title: 'Monitoring Stok',
    type: 'group',
    children: [
        {
            id: 'stok',
            title: 'Monitoring Stok',
            type: 'item',
            url: '/stok',
            icon: icons.IconBuildingWarehouse,
            breadcrumbs: false
        }
    ]
}
 
export default monitoringStock;