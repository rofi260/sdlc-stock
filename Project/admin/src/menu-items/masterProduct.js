// assets
import { IconDatabase } from '@tabler/icons';

// constant
const icons = { IconDatabase };

const masterProduct = {
    id: 'product',
    title: 'Master Produk',
    type: 'group',
    children: [
        {
            id: 'product',
            title: 'Master Produk',
            type: 'item',
            url: '/product',
            icon: icons.IconDatabase,
            breadcrumbs: false
        }
    ]
};

export default masterProduct;