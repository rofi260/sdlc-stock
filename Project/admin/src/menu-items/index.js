import dashboard from './dashboard';
import pages from './pages';
import utilities from './utilities';
import other from './other';
import masterProduct from './masterProduct'
import monitoringStock from './monitoringStock';
import logStock from './logStock';

// ==============================|| MENU ITEMS ||============================== //

const menuItems = {
    items: [dashboard,masterProduct,monitoringStock,logStock, ]
};

export default menuItems;
