import {createSlice,createAsyncThunk} from "@reduxjs/toolkit"
import axios from "axios";
import stockService from "services/stockService"

const API_URL = "http://localhost:8080/admin/stocks";

export const getStocks = createAsyncThunk("stock/getStock",async(param)=>{
    let url = API_URL+"?page=[page]";
    url = url.replace('[page]', Number.parseInt(param.page));
    const response = axios.get(url).then((response) => response.data);
    return response;
}); 

export const addStock = createAsyncThunk("stock/addStock",async({stock_date,description,status})=>{
    const res = await stockService.create({
      stock_date,
      description,
      status});
    return res.data;
}); 

export const updateStock = createAsyncThunk(
    "stock/update",
    async ({ id, data }) => {
      const res = await stockService.update(id, data);
      return res.data;
    }
  );

  export const deleteStock = createAsyncThunk(
    "stock/delete",
    async ({ id }) => {
      await stockService.remove(id);
      return { id };
    }
  );

const initialState = {
    stock : null,
    totalPages:0,
    currentPage:0

}


const stockSlice = createSlice({
    name: "stock",
    initialState: initialState,
    extraReducers:{
        [getStocks.fulfilled] : (state,{payload})=>{
            state.stock=payload.stocks
            state.totalPages=payload.totalPages
            state.currentPage=payload.currentPage
        },
        [addStock.fulfilled]: (state, action) => {
          state.stockData=action.payload;
        },
        [updateStock.fulfilled]: (state, action) => {
        state.stock=action.payload;
        },
        [deleteStock.fulfilled]: (state, action) => {
            let index = state.findIndex(({ id }) => id === action.payload.id);
            state.splice(index, 1);
          },
    }
});

// export const {getStocks} = stockSlice.actions
export default stockSlice.reducer;