import {createSlice,createAsyncThunk} from "@reduxjs/toolkit"
import axios from "axios";
import stockDetailService from "services/stockDetailService";

const API_URL = "http://localhost:8080/admin/stockdetail";

export const getStockDetail = createAsyncThunk("stockDetail/getStockDetail",async(param)=>{
    let url = API_URL+"?page=[page]";
    url = url.replace('[page]', Number.parseInt(param.page));
    const response = axios.get(url).then((response) => response.data);
    return response;
});
export const getSearchStockDetail = createAsyncThunk("stockDetail/getSearchStockDetail",async(param)=>{
    let url = API_URL+"/cari?page=[page]&search=[search]";
    url = url.replace('[search]', String(param.search))
    url = url.replace('[page]', Number.parseInt(param.page));
    const response = axios.get(url).then((response) => response.data);
    return response;
});
export const getPrintStockDetail = createAsyncThunk("stockDetail/getPrintStockDetail",async(param)=>{
    let url = API_URL+"/print?dateFrom=[dateFrom]&dateTo=[dateTo]";
    url = url.replace('[dateFrom]', String(param.dateFrom))
    url = url.replace('[dateTo]', String(param.dateTo));
    const response = axios.get(url).then((response) => response.data);
    return response;
});
export const getLogStockDetail = createAsyncThunk("stockDetail/getLogStockDetail",async(param)=>{
    let url = API_URL+"/[id]"+"?page=[page]";
    url = url.replace('[id]', String(param.id));
    url = url.replace('[page]', Number.parseInt(param.page));
    const response = axios.get(url).then((response) => response.data);
    return response;
});
export const addStockDetail = createAsyncThunk("stockDetail/addStockDetail",async({price,product_id,exp_date,stock_id,current})=>{
    const res = await stockDetailService.create({
        price,
        product_id,
        exp_date,
        stock_id,
        current });
    return res.data;
}); 
export const takeStockDetail = createAsyncThunk("stockDetail/takeStockDetail",async({price,product_id,exp_date,stock_id,current})=>{
    const res = await stockDetailService.takeLogStock({
        price,
        product_id,
        exp_date,
        stock_id,
        current });
    return res.data;
}); 
export const deleteStockDetail = createAsyncThunk("stockDetail/takeStockDetail",async (id) => {
		const response =  stockDetailService.deleteLogStock(id);
		return response.data;
	});



const initialStateStock = {
    stockDetails : null,
    totalPages:0,
    currentPage:0

}


const stockDetailSlice = createSlice({
    name: "stockDetail",
    initialState: {initialStateStock, logProduct:[]},
    reducers:{
        addItem: (state, action) => {
            state.logProduct.push(action.payload)
            
        },
        clearItem:(state,action) => {
            state.logProduct = []
        },
        deleteItem: (state, action) => {
            state.logProduct = state.logProduct.filter(item => item.product_id !== action.payload.product_id)
        },

    },
    extraReducers:{
        [getStockDetail.fulfilled] : (state,{payload})=>{
            state.stockDetails=payload.stockDetails
            state.totalPages=payload.totalPages
            state.currentPage=payload.currentPage
        },
        [addStockDetail.fulfilled]: (state, action) => {
            state.stockData=action.payload;
        },
        [getPrintStockDetail.fulfilled]: (state, {payload}) => {
            state.stockDataPrint=payload;
        },
        [getSearchStockDetail.fulfilled] : (state,{payload})=>{
            state.stockSearchDetails=payload.stockDetails
            state.totalSearchPages=payload.totalPages
            state.currenSearchPage=payload.currentPage
        },
        [getLogStockDetail.fulfilled] : (state,{payload})=>{
            state.stockLogDetails=payload.stockDetails
            state.totalLogPages=payload.totalPages
            state.currentLogPage=payload.currentPage
        },
        [takeStockDetail.fulfilled] : (state,action)=>{
            state.stockData=action.payload;
        },
        [deleteStockDetail.fulfilled]: () => {},
       
    }
});

export const {addItem,clearItem,deleteItem} = stockDetailSlice.actions
export default stockDetailSlice.reducer;