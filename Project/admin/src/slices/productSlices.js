import {createSlice,createAsyncThunk} from "@reduxjs/toolkit"
import axios from "axios";
import productService from "services/productService";

const API_URL = "http://localhost:8080/admin/product";

export const getProducts = createAsyncThunk("product/getProduct",async(param)=>{
    let url = API_URL+"?page=[page]";
    url = url.replace('[page]', Number.parseInt(param.page));
    const response = axios.get(url).then((response) => response.data);
    return response;
}); 
// get id and name product
export const getProductData = createAsyncThunk("product/getProductData",async()=>{
  let url = API_URL+"/data";
  const response = axios.get(url).then((response) => response.data);
  return response;
});

export const addProducts = createAsyncThunk("product/addProduct",async({name,price,expDate})=>{
    const res = await productService.create({
        name,
        price,
        expDate });
    return res.data;
}); 

export const updateProduct = createAsyncThunk(
    "product/update",
    async ({ id, data }) => {
      const res = await productService.update(id, data);
      return res.data;
    }
  );

  export const deleteProduct = createAsyncThunk(
    "product/delete",
    async ({ id }) => {
      await productService.remove(id);
      return { id };
    }
  );

const initialState = {
    product : null,
    totalPages:0,
    currentPage:0

}


const productSlice = createSlice({
    name: "product",
    initialState: initialState,
    extraReducers:{
        [getProducts.fulfilled] : (state,{payload})=>{
            state.product=payload.products
            state.totalPages=payload.totalPages
            state.currentPage=payload.currentPage
        },
        [getProductData.fulfilled] : (state,{payload})=>{
          state.productData=payload
      },
        [addProducts.fulfilled]: (state, action) => {
            state.push(action.payload);
          },
        [updateProduct.fulfilled]: (state, action) => {
        const index = state.findIndex(product => product.id === action.payload.id);
        state[index] = {
            ...state[index],
            ...action.payload,
        };
        },
        [deleteProduct.fulfilled]: (state, action) => {
            let index = state.findIndex(({ id }) => id === action.payload.id);
            state.splice(index, 1);
          },
    }
});

// export const {getProducts} = productSlice.actions
export default productSlice.reducer;