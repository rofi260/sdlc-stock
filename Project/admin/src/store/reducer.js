
// reducer import
import customizationReducer from './customizationReducer';
import authReducer from "../slices/auth";
import messageReducer from "../slices/message";
import productReducer from "../slices/productSlices"
import stockReducer from "../slices/stockSlices"
import stockDetailReducer from "../slices/stockDetailSlices"
import { combineReducers } from 'redux';

// ==============================|| COMBINE REDUCER ||============================== //

const reducer = combineReducers({
    customization: customizationReducer,
    authReducer:authReducer,
    messageReducer:messageReducer,
    productReducer:productReducer,
    stockReducer:stockReducer,
    stockDetailReducer:stockDetailReducer,

});

export default reducer;

