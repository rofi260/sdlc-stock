import React, {  useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
// material-ui
import { useTheme } from '@mui/material/styles';
import {
    Chip,
} from '@mui/material';
import { logout } from  "../../../../slices/auth";
// assets
import { IconLogout, } from '@tabler/icons';
import { Navigate } from "react-router";

// ==============================|| PROFILE MENU ||============================== //

const ProfileSection = () => {

    const theme = useTheme();

    
    const { user: currentUser } = useSelector((state) => state.authReducer);
    const dispatch = useDispatch();
    const logOut = useCallback(() => {
        dispatch(logout());
    }, [dispatch]);
    if (!currentUser) {
        return <Navigate to="/pages/login" />;
    }
    return (
        <>
            <Chip
                sx={{
                    height: '48px',
                    alignItems: 'center',
                    borderRadius: '27px',
                    transition: 'all .2s ease-in-out',
                    borderColor: theme.palette.primary.light,
                    backgroundColor: theme.palette.primary.light,
                    '&[aria-controls="menu-list-grow"], &:hover': {
                        borderColor: theme.palette.primary.main,
                        background: `${theme.palette.primary.main}!important`,
                        color: theme.palette.primary.light,
                        '& svg': {
                            stroke: theme.palette.primary.light
                        }
                    },
                    '& .MuiChip-label': {
                        lineHeight: 0
                    }
                }}
                
                label={<IconLogout stroke={1.5} size="1.5rem" color={theme.palette.primary.main} />}
                variant="outlined"
                aria-haspopup="true"
                onClick={logOut}
                color="primary"
            />
            
        </>
    );
};

export default ProfileSection;
