// import { Link } from 'react-router-dom';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Divider, Grid, Paper, Stack, ThemeProvider, Typography, useMediaQuery } from '@mui/material';

// project imports
// import AuthWrapper1 from '../AuthWrapper1';
import AuthCardWrapper from '../AuthCardWrapper';
import AuthLogin from '../auth-forms/AuthLogin';
import Logo from 'ui-component/Logo';

import imageBG from '../../../../assets/images/gedung_nexsoft.jpg';

// assets

// ================================|| AUTH3 - LOGIN ||================================ //

const Login = () => {
    const theme = useTheme();
    const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));

    return (
        <ThemeProvider theme={theme}>
            <Grid container component="main" sx={{ height: '100vh',backgroundColor: theme.palette.primary.dark, }}>
                <Grid 
                item xs={false} 
                sm={4} md={7} 
                sx={{backgroundImage: `url(${imageBG})`,
                    backgroundRepeat: 'no-repeat',
                    // backgroundColor: theme.palette.primary.light,
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',}}
                    />
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square sx={{ backgroundColor: theme.palette.primary.light, }}>
                    <Grid container justifyContent="center" alignItems="center" sx={{ minHeight: '100vh' }}>
                        <Grid >
                            <AuthCardWrapper>
                                <Grid container alignItems="center" justifyContent="center">
                                    <Grid >
                                        {/* <Link to="#">
                                        </Link> */}
                                        <Logo />
                                    </Grid>
                                    <Grid item xs={12}>
                                      <Divider sx={{ mb: 3 }}/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Grid
                                            container
                                            direction={matchDownSM ? 'column-reverse' : 'row'}
                                            alignItems="center"
                                            justifyContent="center"
                                        >
                                            <Grid>
                                                <Stack alignItems="center" justifyContent="center" spacing={1}>
                                                    <Typography
                                                        color={theme.palette.primary[900]}
                                                        gutterBottom
                                                        variant={matchDownSM ? 'h3' : 'h2'}
                                                    >
                                                        Halaman Login
                                                    </Typography>
                                                    <Typography
                                                        variant="caption"
                                                        fontSize="16px"
                                                        textAlign={matchDownSM ? 'center' : 'inherit'}
                                                    >
                                                        ND'6 Admin Stok
                                                    </Typography>
                                                </Stack>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    
                                    <Grid item xs={12}>
                                        <AuthLogin />
                                    </Grid>
                                </Grid>
                            </AuthCardWrapper>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </ThemeProvider>
        
    );
};

export default Login;
