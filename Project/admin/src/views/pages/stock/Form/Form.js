import  React, {useState} from 'react';

import { 
    TextField,
    Grid,
    Button,
    } from '@mui/material';
import { addStock} from '../../../../slices/stockSlices';
import SubCard from 'ui-component/cards/SubCard';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
// import { maxWidth } from '@mui/system';

    
const Form = () => {

    const dispatch=useDispatch();

    const initialStockState = {
    stock_date: "",
    description: "",
    status: "",
    };
    const [stock, setStock] = useState(initialStockState);
    const navigation = useNavigate()
    const handleInputChange = event => {
        const { name, value } = event.target;
        setStock({ ...stock, [name]: value });
    };
    const saveStock = () => {
        const { stock_date, description,status } = stock;
        dispatch(addStock({ stock_date, description,status }))
            .unwrap()
            .then(data => {
                navigation('/stok/detail/'+data.id)
            })
            .catch(e => {
            console.log(e);
            });
           
        //;
    };
    
    const status = [
        "IN",
        "OUT",
    ];

    return ( 
        <Grid item >
            <SubCard >
                <Grid 
                    container
                    spacing={3}
                    direction="row"
                    justifyContent="space-between"
                >
                   
                    <Grid 
                    
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="status"
                            name="status"
                            onChange={handleInputChange}
                            value={stock.status || ''}
                            variant="outlined"
                            select
                            SelectProps={{
                                native: true,
                              }}
                        >
                            <option aria-label="None" value="" />
                            {status?.map((option) => (
                                <option key={option} value={option}>
                                {option}
                                </option>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid 
                    item 
                    md={6}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Tanggal dibuat"
                            name="stock_date"
                            type="date"
                            onChange={handleInputChange}
                            value={stock.stock_date || ''}
                            required
                            InputLabelProps={{ shrink: true}}
                        />
                    </Grid>
                    <Grid 
                    item
                    md={12}
                    xs={12}>
                        <TextField
                        fullWidth
                        label="Deskripsi Stok"
                        name="description"
                        onChange={handleInputChange}
                        value={stock.description || ''}
                        required
                        variant="outlined"
                        multiline
                        // InputLabelProps={{ shrink: true}}
                        >

                        </TextField>
                    </Grid>
                    <Grid
                    item
                    container 
                    justifyContent="flex-end"
                    >
                        <Button 
                         color="info"
                          variant="contained"
                          onClick={saveStock}
                          >Tambahkan</Button>
                    </Grid>
                </Grid>
            </SubCard>
        </Grid >
     );
}
 
export default Form;