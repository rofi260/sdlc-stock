import React, {useEffect, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux'; 
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Box,
    Card,
    CardContent,
    TextField,
    InputAdornment,
    SvgIcon,
    Grid,Button,IconButton, Tooltip, Pagination, Chip 
    } from '@mui/material';

import SubCard from 'ui-component/cards/SubCard';
// import icon
import {  useTheme } from '@mui/material/styles';
import { IconEye} from "@tabler/icons";
import Popup from './Form/PopUp';
import Form from './Form/Form';
import { getStocks,deleteStock } from '../../../slices/stockSlices';
import { Navigate } from 'react-router';
import { Link } from 'react-router-dom';


const warna = {
    color:'black'
}



const StockTable = () => {
    const dispatch = useDispatch();
    const stocks = useSelector((state) => state.stockReducer.stock)
    const currpage = useSelector((state) => state.stockReducer.currentPage)
    const count = useSelector((state) => state.stockReducer.totalPages)
    const [page, setPage] = React.useState(1);
    // const [isEdit, setIsEdit] = React.useState(false); off karena masih fail Dialog untuk add dan edit
  
    const handleChangePage = ( event, newPage) => {
		setPage(newPage);
	};


    useEffect(() => {
        getAllStock();
    }, [dispatch,page, count]);

    const getAllStock = () => {
		const param = {
			page: page-1,
		};
		dispatch(getStocks(param));
	};
    
    

    const [openPopup,setOpenPopup] = React.useState(false)
    const theme = useTheme();
    return ( 
        <><Box>
            {/* Search component */}
            <Box sx={{ mt: 3 }}>
                <Card>
                    <CardContent>
                        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                            <Box sx={{ maxWidth: 500 }}>
                                {/* <TextField
                                    fullWidth
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <SvgIcon
                                                    color="action"
                                                    fontSize="small"
                                                >
                                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-search" width="25" height="25" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#597e8d" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                        <circle cx="10" cy="10" r="7" />
                                                        <line x1="21" y1="21" x2="15" y2="15" />
                                                    </svg>
                                                </SvgIcon>
                                            </InputAdornment>
                                        )
                                    }}
                                    placeholder="Search Produk"
                                    variant="outlined" /> */}
                            </Box>
                            <Box sx={{ m: 1 }}>
                                <Button
                                    color="secondary"
                                    variant="contained"
                                    sx={{ backgroundColor: theme.palette.secondary.main, }}
                                    onClick= {() => setOpenPopup(true)}
                                >
                                    Tambah Stok
                                </Button>
                            </Box>
                        </Box>
                    </CardContent>
                </Card>
            </Box>
            <Box sx={{ mt: 3 }}>
                <Grid>
                    <SubCard>
                        <TableContainer component={Paper}>
                            <Table size="small" aria-label="a dense table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell sx={warna}>
                                            ID Produk
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Deskripsi
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Tanggal Dibuat
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Jenis stok
                                        </TableCell>
                                        <TableCell align="center" sx={warna}>
                                            Aksi
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {stocks?.map((detail) => (
                                        <TableRow
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                            key={detail.id}
                                        >
                                            <TableCell sx={warna}>
                                                {detail.id}
                                            </TableCell>
                                            <TableCell sx={warna}>
                                                {detail.description}
                                            </TableCell>
                                            <TableCell sx={warna}>
                                                {detail.stock_date}
                                            </TableCell>
                                            <TableCell
                                                sx={warna}>
                                                    <Chip  sx={{color:'#ffffff'}}  label={detail.status} color={detail.status === 'IN'? 'success' : detail.status === 'OUT' ? 'error' : 'primary'} />
                                                
                                            </TableCell>
                                            <TableCell align="center" sx={warna}>
                                                <Tooltip describeChild title="Klik untuk melihat lebih detail Stok ini" arrow>
                                                    <Link to={"/stok/detail/"+detail.id}>
                                                    <IconButton
                                                       
                                                        sx={{ color: theme.palette.primary.main }}
                                                    >
                                                        <IconEye />
                                                    </IconButton>
                                                    </Link>
                                                </Tooltip>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <Pagination
                                sx={{ mt: 2 }}
                                    color="primary"
                                    count={count}
                                    page={page}
                                    variant="outlined"
                                    size="small"
                                    onChange={handleChangePage}
                                    shape="rounded"
                                    />
                    </SubCard>
                </Grid>
            </Box>
        </Box>
        <Popup
            title="Form Tambah Produk"
            openPopup={openPopup}
            setOpenPopup={setOpenPopup}
        >
            <Form />
        </Popup>
        </>
        
     );
}
 
export default StockTable;