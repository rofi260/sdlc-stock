import StockHeader from "./StockHeader";
import StockTable from "./StockTable";

// material-ui
import { Box,
    Container,} from '@mui/material';
const Stock = () => {
    return ( 
        <Box
        component="main"
        sx={{
            flexGrow: 1,
        }}
        >
            <Container maxWidth={false}>
                <StockHeader/>
                <Box sx={{ mt: 3 }}>
                    <Box>
                        <StockTable/>
                    </Box>
                </Box>
                
            </Container>
        </Box>
     );
}
 
export default Stock;