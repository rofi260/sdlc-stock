import * as React from 'react';

import { 
    TextField,
    Grid,
    Button,
    } from '@mui/material';

import SubCard from 'ui-component/cards/SubCard';
// import { maxWidth } from '@mui/system';

const Form = () => {
    
    return ( 
        <Grid item >
            <SubCard >
                <Grid 
                    container
                    spacing={3}
                    direction="row"
                    justifyContent="space-between"
                    alignItems="flex-end"
                >
                    <Grid 
                    
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Nama Produk"
                            name="nameProduct"
                            // onChange={handleChange}
                            required
                            variant="outlined"
                        />
                    </Grid>
                    <Grid 
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Merek Produk"
                            name="brandProduct"
                            // onChange={handleChange}
                            required
                            variant="outlined"
                        />
                    </Grid>
                    <Grid 
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Harga Produk"
                            name="priceProduct"
                            // onChange={handleChange}
                            required
                            variant="outlined"
                        />
                    </Grid>
                    <Grid 
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Tanggal kadaluarsa"
                            name="stockCreated"
                            type="date"
                            // onChange={handleChange}
                            required
                            variant="outlined"
                            InputLabelProps={{ shrink: true}}
                        />
                    </Grid>
                    <Grid 
                    item
                    md={12}
                    xs={12}>
                        <TextField
                        fullWidth
                        label="Deskripsi Stok"
                        name="descStock"
                        // onChange={handleChange}
                        required
                        variant="outlined"
                        multiline
                        // InputLabelProps={{ shrink: true}}
                        >

                        </TextField>
                    </Grid>
                    <Grid
                    item
                    container 
                    justifyContent="flex-end"
                    >
                        <Button  color="info" variant="contained">Tambahkan</Button>
                    </Grid>
                </Grid>
            </SubCard>
        </Grid >
     );
}
 
export default Form;