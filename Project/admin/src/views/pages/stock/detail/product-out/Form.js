import  React, {useState} from 'react';
import { useParams } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { takeStockDetail} from '../../../../../slices/stockDetailSlices';
import { addItem} from '../../../../../slices/stockDetailSlices';

import { 
    TextField,
    Grid,
    Button,
    Alert,
    } from '@mui/material';

import SubCard from 'ui-component/cards/SubCard';
// import { maxWidth } from '@mui/system';

const Form = () => {
    const id = useParams();
    const dispatch=useDispatch();
    const dataProduct = useSelector(state => state.productReducer.productData)
    const initialProductDetailState = {
        
        price:"",
        product_id:"",
        exp_date:"",
        stock_id: id.id,
        current:"",
        status:"OUT"
      };
    const [submitted, setSubmitted] = useState(false);
    const [stockDetail, setStockDetail] = useState(initialProductDetailState);
    const handleInputChange = event => {
        const { name, value } = event.target;
        setStockDetail({ ...stockDetail, [name]: value });
    };

    const saveStockDetail = () => {
        dispatch(addItem(stockDetail));
        setStockDetail(initialProductDetailState);
    };
    
    return ( 
        <Grid item >
            <SubCard >
                <Grid 
                    container
                    spacing={3}
                    direction="row"
                    justifyContent="space-between"
                    alignItems="flex-end"
                >
                   <Grid 
                    
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Nama Produk"
                            name="product_id"
                            onChange={handleInputChange}
                            value={stockDetail.product_id || ''}
                            required
                            select
                            variant="outlined"
                            SelectProps={{
                                native: true,
                              }}

                        >
                            {dataProduct?.map((option) => (
                                <option key={option.id} value={option.id}>
                                {option.name}
                                </option>
                            ))}
                        </TextField>    
                    </Grid>
                    <Grid 
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Harga"
                            name="price"
                            onChange={handleInputChange}
                            value={stockDetail.price || ''}
                            required
                            variant="outlined"
                            type='number'
                        />
                    </Grid>
                    <Grid 
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Tanggal Keluar"
                            name="exp_date"
                            type="date"
                            onChange={handleInputChange}
                            value={stockDetail.exp_date || ''}
                            required
                            variant="outlined"
                            InputLabelProps={{ shrink: true}}
                        />
                    </Grid>
                    <Grid 
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Jumlah stok keluar"
                            name="current"
                            onChange={handleInputChange}
                            value={stockDetail.current || ''}
                            required
                            variant="outlined"
                            type='number'
                        />
                    </Grid>
                    <Grid
                    item
                    container 
                    justifyContent="flex-end"
                    >
                        <Button
                        color="info"
                        variant="contained"
                        type="submit"
                        onClick={saveStockDetail}
                           >Kirim</Button>
                    </Grid>
                </Grid>
            </SubCard>
        </Grid >
     );
}
 
export default Form;