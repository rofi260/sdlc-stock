import  React, {useState} from 'react';
import { useParams } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { addStockDetail,addItem} from '../../../../../slices/stockDetailSlices';
import productService from 'services/productService';


import { 
    TextField,
    Grid,
    Button,
    Alert,
    MenuItem,
    } from '@mui/material';

import SubCard from 'ui-component/cards/SubCard';
// import { maxWidth } from '@mui/system';

const Form = () => {
    const id = useParams();
    const dispatch= useDispatch();
    const dataProduct = useSelector(state => state.productReducer.productData)
   
    const initialProductDetailState = {
        
        price:null,
        product_id:"",
        exp_date:"",
        stock_id: id.id,
        current:null,
        status:"IN"
      };
    const [submitted, setSubmitted] = useState(false);
    const [stockDetail, setStockDetail] = useState(initialProductDetailState);
    const handleInputChange = event => {
        const { name, value } = event.target;
        setStockDetail({ ...stockDetail, [name]: value });
    };
    
    const saveStockDetail = () => {
        dispatch(addItem(stockDetail));
        setStockDetail(initialProductDetailState);
    };
    
    return ( 
        <Grid item >
            <SubCard >
                <Grid 
                    container
                    spacing={3}
                    direction="row"
                    justifyContent="space-between"
                    alignItems="flex-end"
                >
                    <Grid 
                    
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Nama Produk"
                            select
                            name="product_id"
                            onChange={handleInputChange}
                            required
                            SelectProps={{
                                native: true,
                              }}
                        >
                            <option aria-label="None" value="" />
                            {dataProduct?.map((option) => (
                                <option key={option.id} value={option.id}>
                                {option.name}
                                </option>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid 
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Harga"
                            type='number'
                            name="price"
                            onChange={handleInputChange}
                            value={stockDetail.price || ''}
                            required
                            variant="outlined"
                        />
                    </Grid>
                    <Grid 
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Tanggal Kadaluarsa"
                            name="exp_date"
                            type="date"
                            onChange={handleInputChange}
                            value={stockDetail.exp_date || ''}
                            required
                            variant="outlined"
                            InputLabelProps={{ shrink: true}}
                        />
                    </Grid>
                    <Grid 
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            type='number'
                            fullWidth
                            label="Jumlah stok"
                            name="current"
                            onChange={handleInputChange}
                            value={stockDetail.current || ''}
                            required
                            variant="outlined"
                        />
                    </Grid>
                    <Grid
                    item
                    container 
                    justifyContent="flex-end"
                    >
                        <Button
                        color="info"
                        variant="contained"
                        type="submit"
                        onClick={saveStockDetail}
                           >Tambahkan</Button>
                    </Grid>
                </Grid>
            </SubCard>
        </Grid >
     );
}
 
export default Form;