import  React, {useEffect, useState} from 'react';
import { useParams } from 'react-router';
import { useDispatch } from 'react-redux';
import { updateStock} from '../../../../../slices/stockSlices';
import stockService from '../../../../../services/stockService';

import { 
    TextField,
    Grid,
    Button,
    } from '@mui/material';

import SubCard from 'ui-component/cards/SubCard';
// import { maxWidth } from '@mui/system';

const Form = () => {
    const id = useParams();
    const initialStockState = {
        id: null,
        stock_date: "",
        description: "",
    };
    const [currentStock, setCurrentStock] = useState(initialStockState);
    const dispatch = useDispatch();

    const getStock = id => {
        stockService.get(id)
        .then(response => {
            setCurrentStock(response.data);
        })
        .catch(e => {
            console.log(e);
        });
    };
    useEffect(() => {
        getStock(id.id);
    }, [id.id]);
    
    
    const [message, setMessage] = useState("");
    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentStock({ ...currentStock, [name]: value });
    };
    const tempId=id.id;
    

    const updateData = () => {
        
        dispatch(updateStock({ id: tempId, data: currentStock }))
        
          .unwrap()
          .then(response => {
            console.log(response);
            setMessage("Produk Behasil di Ubah!");
          })
          .catch(e => {
            console.log(e);
          });
      };


    return ( 
        <Grid item >
            <SubCard >
                <Grid 
                    container
                    spacing={3}
                    direction="row"
                    justifyContent="space-between"
                >
                    {/* <Grid 
                    
                    item 
                    md={3}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Stok ID"
                            name="nameProduct"
                            // onChange={handleChange}
                            value="STOK0001"
                            variant="outlined"
                            inputProps={ {readOnly: true,}}
                        />
                    </Grid> */}
                    <Grid 
                    item 
                    md={6}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Tanggal dibuat"
                            name="stock_date"
                            type="date"
                            onChange={handleInputChange}
                            value={currentStock.stock_date || ''}
                            required
                            InputLabelProps={{ shrink: true}}
                        />
                    </Grid>
                    <Grid 
                    item
                    md={12}
                    xs={12}>
                        <TextField
                        fullWidth
                        label="Deskripsi Stok"
                        name="description"
                        onChange={handleInputChange}
                        value={currentStock.description || ''}
                        required
                        variant="outlined"
                        multiline
                        // InputLabelProps={{ shrink: true}}
                        >

                        </TextField>
                    </Grid>
                    <Grid
                    item
                    container 
                    justifyContent="flex-end"
                    >
                        <Button 
                        type="submit" 
                         color="info"
                          variant="contained"
                          onClick={updateData}
                          >Tambahkan</Button>
                    </Grid>
                </Grid>
            </SubCard>
        </Grid >
     );
}
 
export default Form;