import React, { useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { gridSpacing } from 'store/constant';
import { Box,
    TextField,
    Grid,
    Pagination,
    Chip,
    Tooltip,
    IconButton,
    Button,
    } from '@mui/material';
import {  useTheme } from '@mui/material/styles';
import SubCard from 'ui-component/cards/SubCard';
import stockService from 'services/stockService';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from "react-router";
import { getLogStockDetail,deleteStockDetail,deleteItem } from '../../../../slices/stockDetailSlices';
import { updateStock} from '../../../../slices/stockSlices';
import getDataStock from 'services/stockDetailService';
import { IconEdit, IconTrash } from "@tabler/icons";
import { getProductData } from '../../../../slices/productSlices';
import { takeStockDetail,addStockDetail} from '../../../../slices/stockDetailSlices';

const StockDetailData = () => {
    const dispatch = useDispatch();
   
    const logProduct = useSelector(state => state.stockDetailReducer.logProduct)
    const logStock = useSelector((state) => state.stockDetailReducer.stockLogDetails)
   
    const count = useSelector((state) => state.stockDetailReducer.totalLogPages)
    const [page, setPage] = useState(1);
    const [deleteData, setDeleteData] = useState(false);
    const [submit, setSubmit] = useState(false);
    
    
    function DataSelection(){
        if ( logProduct.length>0) {
            return ReduxData();
        } else {
            return DatabaseData();
        }
    }

    function DatabaseData(){
        return <TableBody>
        {logStock?.filter(detail => detail.stock_id ===id.id).map((detail,index)=>(
                <TableRow
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    key={index}
                >
                    <TableCell sx={warna}>
                        {detail.product_id}
                    </TableCell>
                    <TableCell sx={warna}>
                        {detail.exp_date}
                    </TableCell>
                    <TableCell  
                    sx={warna}>
                        {detail.current}
                    </TableCell>
                    <TableCell  sx={warna}>
                        {detail.price.toLocaleString("id-ID", {style:"currency", currency:"IDR"})}
                    </TableCell>
                    <TableCell  
                    sx={warna}>
                        <Chip  sx={{color:'#ffffff'}}  label={detail.status} color={detail.status === 'IN'? 'success' : detail.status === 'OUT' ? 'error' : 'primary'} />
                    </TableCell>
                    <TableCell align="center" sx={warna} >
                            <IconButton 
                            disabled
                            onClick= {() => DeleteStockDetail(detail.id)}
                            sx={{ color: theme.palette.error.main }}>
                                <IconTrash />
                            </IconButton>
                    </TableCell>
                </TableRow>
                ))}
            </TableBody>
    }

    function ReduxData(){
        return <TableBody>
        {logProduct?.filter(detail => detail.stock_id ===id.id).map((detail,index)=>(
                <TableRow
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    key={index}
                >
                    <TableCell sx={warna}>
                        {detail.product_id}
                    </TableCell>
                    <TableCell sx={warna}>
                        {detail.exp_date}
                    </TableCell>
                    <TableCell  
                    sx={warna}>
                        {detail.current}
                    </TableCell>
                    <TableCell  sx={warna}>
                        {detail.price?.toLocaleString("id-ID", {style:"currency", currency:"IDR"})}
                    </TableCell>
                    <TableCell  
                    sx={warna}>
                        <Chip  sx={{color:'#ffffff'}}  label={detail.status} color={detail.status === 'IN'? 'success' : detail.status === 'OUT' ? 'error' : 'primary'} />
                    </TableCell>
                    <TableCell align="center" sx={warna}>
                                <Tooltip describeChild title="Tekan untuk menunggah data" arrow>
                                    <IconButton
                                        onClick= {() => uploadStockDetail(detail)}
                                        sx={{ color: theme.palette.warning.dark }}
                                    >
                                        <IconEdit />
                                    </IconButton>
                                </Tooltip>
                                <Tooltip describeChild title="Tekan untuk menghapus data" arrow>
                                    <IconButton 
                                    onClick={() => deleteDataFunction(detail)}
                                    sx={{ color: theme.palette.error.main }}>
                                        <IconTrash />
                                    </IconButton>
                                </Tooltip>
                            </TableCell>
                </TableRow>
                ))}
            </TableBody>
    }
 
    
    function uploadStockDetail (detail) {
       
        if (detail.status=="IN") {
            const { price, product_id, exp_date, stock_id,current } = detail;
            dispatch(addStockDetail({ price, product_id, exp_date, stock_id,current }))
            .unwrap()
            .then(data => {
                console.log("berhasil upload");
                deleteDataFunction(detail)
            })
            .catch(e => {
                console.log(e);
            });
        } else {
            const { price, product_id, exp_date, stock_id,current } = detail;
            dispatch(takeStockDetail({ price, product_id, exp_date, stock_id,current }))
            .unwrap()
            .then(data => {
                console.log("berhasil upload");
                deleteDataFunction(detail)
            })
            .catch(e => {
                console.log(e);
            });
        }
        // dispatch(addItem(stockDetail));
    };

    function deleteDataFunction(detail) {
        dispatch(deleteItem(detail))
        console.log(detail);
    }

    const handleChangePage = ( event, newPage) => {
		setPage(newPage);
	};
   
    const getAllstockDetail = () => {
        
		const param = {
			page: page-1,
            id: id.id
		};
		dispatch(getLogStockDetail(param));
	};

    const theme = useTheme();
    const id = useParams();
    const initialStockState = {
        id: "",
        stock_date: "",
        description: "",
        status:"",
    };
    const [currentStock, setCurrentStock] = useState(initialStockState);
    const handleEditChange = event => {
        const { name , value } = event.target;
        setCurrentStock({ ...currentStock, [name]: value});
    }
    const getStock = id => {
        stockService.get(id)
        .then(response => {
            setCurrentStock(response.data);
        })
        .catch(e => {
            console.log(e);
        });
    };
    useEffect(() => {
        getStock(id.id);
        getAllstockDetail();
        getStockDetailData(id.id);
        getDataProduct();
        setDeleteData(false);
        setSubmit(false)
    }, [id.id,dispatch,page, count,deleteData,submit]);

    const initialStockDetailData = {
        current:"",
        product:"",
        untung:"",
    }

    const [stockDetailData, setStockDetailData ] = useState(initialStockDetailData);
    const getStockDetailData = id => {
        getDataStock.getDataStock(id)
        .then(response => {
            setStockDetailData(response.data);
        })
        .catch(e => {
            console.log(e);
        });
    };
    const getDataProduct = () => {
        dispatch(getProductData());
    };

    const DeleteStockDetail= (id) =>{
        dispatch(deleteStockDetail(id))
        setDeleteData(true);
    };
    
    const updateData = () => {
    //    console.log(currentStock)
        dispatch(updateStock({ id: id.id, data: currentStock }))
        
          .unwrap()
          .then(response => {
            console.log(response);
            setSubmit(true);
          })
          .catch(e => {
            console.log(e);
          });
      };
    
    
    const warna = {
        color:'black'
    }
    return ( 
        <Box>
            <Box sx={{ mt: 3 }}>
                <Grid container spacing={gridSpacing}>
                    <Grid item lg={9} md={8} sm={8} xs={12}>
                        <SubCard title="Transaction header">
                            <Grid 
                                container
                                spacing={3}
                                direction="row"
                                justifyContent="space-between"
                                alignItems="flex-end"
                            >
                                <Grid 
                                
                                item 
                                md={2}
                                xs={12}>
                                    <TextField
                                        fullWidth
                                        label="Stok ID"
                                        name="id"
                                        // onChange={handleEditChange}
                                        required
                                        value={currentStock.id}
                                        variant="outlined"
                                        inputProps={ { readOnly: true} }
                                    />
                                </Grid>
                                <Grid 
                                
                                item 
                                md={2}
                                xs={12}>
                                    <TextField
                                        fullWidth
                                        label="Jenis Stok"
                                        name="status"
                                        // onChange={handleEditChange}
                                        required
                                        value={currentStock.status}
                                        variant="outlined"
                                        inputProps={ {readOnly: true,}}
                                    />
                                </Grid>
                                <Grid 
                                item 
                                md={4}
                                xs={12}>
                                    <TextField
                                        fullWidth
                                        label="Stok dibuat"
                                        name="stock_date"
                                        onChange={handleEditChange}
                                        required
                                        type="date"
                                        value={currentStock.stock_date}
                                        variant="outlined"
                                    />
                                </Grid>
                                <Grid
                                item 
                                md={2}
                                xs={12}>
                                    <Box sx={{ m: 1 }}>
                                            <Button
                                                color="primary"
                                                variant="contained"
                                                sx={{ backgroundColor: theme.palette.primary.main, color: '#fff' }}
                                                onClick= {() => updateData()}
                                            >
                                                Simpan
                                            </Button>
                                        </Box>
                                </Grid>
                                <Grid 
                                item
                                md={12}
                                xs={12}>
                                    <TextField
                                    fullWidth
                                    label="Deskripsi Stok"
                                    name="description"
                                    onChange={handleEditChange}
                                    required
                                    value={currentStock.description}
                                    variant="outlined"
                                    multiline
                                    minRows={5}
                                    >

                                    </TextField>
                                </Grid>
                            </Grid>
                        </SubCard>
                    </Grid >
                    <Grid item lg={3} md={4} sm={4} xs={12}>
                        <SubCard title="Transaction Total">
                            <Grid 
                                container
                                spacing={3}
                                direction="column"
                            >
                                <Grid 
                                
                                item >
                                    <TextField
                                        fullWidth
                                        label="Total Produk"
                                        name="product"
                                        // onChange={handleChange}
                                        value={stockDetailData.product}
                                        variant="outlined"
                                        inputProps={ {readOnly: true,}}
                                    />
                                </Grid>
                                <Grid 
                                item >
                                    <TextField
                                        fullWidth
                                        label="Total Quantity Produk"
                                        name="current"
                                        // onChange={handleChange}
                                        value={stockDetailData.current}
                                        variant="outlined"
                                        inputProps={{ readOnly: true}}
                                    />
                                </Grid>
                                <Grid 
                                item>
                                    <TextField
                                    fullWidth
                                    label="Total Harga Produk"
                                    name="untung"
                                    // onChange={handleChange}
                                    value={stockDetailData.untung?.toLocaleString("id-ID", {style:"currency", currency:"IDR"})}
                                    variant="outlined"
                                    inputProps={{ readOnly: true}}
                                    
                                    >

                                    </TextField>
                                </Grid>
                            </Grid>
                        </SubCard>
                    </Grid >
                </Grid>
            </Box>

            {/* New Product*/}
           
            {/* Transactional table */}
            <Box sx={{ mt: 3 }}>
                <Grid>
                    <SubCard title="Transaction Total">
                        <TableContainer  component={Paper}>
                            <Table  size="small" aria-label="a dense table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell sx={warna}>
                                            ID Produk
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Kadaluarsa
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Quantity Stok
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Harga
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Status Data
                                        </TableCell>
                                        <TableCell align="center" sx={warna}>
                                            Action
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <DataSelection/>
                                    
                            </Table>
                            <Pagination
                                    sx={{ mt: 2 }}
                                        color="primary"
                                        count={count}
                                        page={page}
                                        variant="outlined"
                                        size="small"
                                        onChange={handleChangePage}
                                        shape="rounded"
                                        />
                        </TableContainer>
                    </SubCard>
                </Grid>
            </Box>
        </Box>
        
     );
}
 
export default StockDetailData;