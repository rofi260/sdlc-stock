// material-ui
import * as React from 'react';
import { Box, Button, Container, Typography } from '@mui/material';
import StockDetailData from './StockDetailData';
import { useTheme } from '@mui/material/styles';
import Popup from './form-detail/PopUp';
import Form from './form-detail/Form';
// edit dialog
import EditPopup from './edit-dialog/PopUp';
import EditForm from './edit-dialog/Form';
// dialog form add Product in
import ProductIn from './product-in/Form';
import ProductInPopup from './product-in/PopUp';
// dialog form add Product out
import ProductOut from './product-out/Form';
import ProductOutPopup from './product-out/PopUp';
import stockService from 'services/stockService';
import { useParams } from 'react-router';

const StockDetail = () => {
    const [openPopup, setOpenPopup] = React.useState(false);
    const [openEditPopup, setOpenEditPopup] = React.useState(false);
    const [openProdInPopup, setOpenProdInPopup] = React.useState(false);
    const [openProdOutPopup, setOpenProdOutPopup] = React.useState(false);
    const theme = useTheme();
    const editFunct = React.useRef(null);
    const initialStockState = {
        id: '',
        stock_date: '',
        description: '',
        status: ''
    };
    const id = useParams();
    const [currentStock, setCurrentStock] = React.useState(initialStockState);
    const getStock = (id) => {
        stockService
            .get(id)
            .then((response) => {
                setCurrentStock(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    };

    React.useEffect(() => {
        getStock(id.id);
    }, []);
    function StockType() {
        if (currentStock.status == 'IN') {
            return ShowButtonIn();
        } else {
            return ShowButtonOut();
        }
    }

    function ShowButtonIn() {
        return (
            <Button
                color="success"
                variant="contained"
                sx={{ backgroundColor: theme.palette.success.main }}
                onClick={() => setOpenProdInPopup(true)}
            >
                Produk Masuk
            </Button>
        );
    }

    function ShowButtonOut() {
        return (
            <Button
                color="success"
                variant="contained"
                sx={{ backgroundColor: theme.palette.success.main }}
                onClick={() => setOpenProdOutPopup(true)}
            >
                Produk Keluar
            </Button>
        );
    }

    return (
        <>
            <Box
                component="main"
                sx={{
                    flexGrow: 1
                }}
            >
                <Container maxWidth={false}>
                    {/* Header */}

                    <Box>
                        <Box
                            sx={{
                                display: 'flex',
                                justifyContent: 'space-between',
                                flexWrap: 'wrap',
                                m: -1
                            }}
                        >
                            <Typography
                                sx={{
                                    m: 1
                                }}
                                variant="h2"
                            >
                                Monitoring Stok
                            </Typography>
                            <Box sx={{ display: 'flex', justifyContent: 'end' }}>
                                <Box sx={{ m: 1 }}>
                                    <StockType />
                                </Box>
                                {/* <Box sx={{ m: 1 }}>
                                <Button
                                    color="primary"
                                    variant="contained"
                                    sx={{ backgroundColor: theme.palette.grey[400], color: '#fff' }}
                                    onClick= {() => editFunct.current()}
                                >
                                    Ubah Stok
                                </Button>
                            </Box> */}
                            </Box>
                        </Box>
                    </Box>
                    {/* End of Header */}

                    {/* Header Detail Stock */}
                    <Box sx={{ mt: 3 }}>
                        <Box>
                            <StockDetailData editFunct={editFunct} />
                        </Box>
                    </Box>
                </Container>
            </Box>
            <Popup title="Form Tambah Produk" openPopup={openPopup} setOpenPopup={setOpenPopup}>
                <Form />
            </Popup>
            <EditPopup title="Form Ubah Stok" openEditPopup={openEditPopup} setOpenEditPopup={setOpenEditPopup}>
                <EditForm />
            </EditPopup>
            <ProductInPopup title="Form Produk Masuk" openProdInPopup={openProdInPopup} setOpenProdInPopup={setOpenProdInPopup}>
                <ProductIn />
            </ProductInPopup>
            <ProductOutPopup title="Form Produk Keluar" openProdOutPopup={openProdOutPopup} setOpenProdOutPopup={setOpenProdOutPopup}>
                <ProductOut />
            </ProductOutPopup>
        </>
    );
};

export default StockDetail;
