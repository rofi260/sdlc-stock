// material-ui
import { Box,
    // Button,
     Typography } from '@mui/material';
    

// project imports


const StockHeader= () => {
    return ( 
        <Box>
            <Box
                sx={{
                    alignItems: 'center',
                    display: 'flex',
                    justifyContent: 'space-between',
                    flexWrap: 'wrap',
                    m: -1
                }}
            >
                <Typography
                    sx={{
                        m: 1
                    }}
                    variant="h2"
                >
                    Monitoring Stok
                </Typography>
            </Box>
        </Box>
     );
}
 
export default StockHeader;