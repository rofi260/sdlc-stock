import { Box, Typography } from "@mui/material";


const LogHeader = () => {
    return ( 
        <Box>
            <Box
                sx={{
                    alignItems: 'center',
                    display: 'flex',
                    justifyContent: 'space-between',
                    flexWrap: 'wrap',
                    m: -1
                }}
            >
                <Typography
                    sx={{
                        m: 1
                    }}
                    variant="h2"
                >
                    Log Stok Produk
                </Typography>
            </Box>
        </Box>
     );
}
 
export default LogHeader;