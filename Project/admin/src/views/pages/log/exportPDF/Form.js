import  React, {useEffect, useRef, useState} from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { getPrintStockDetail} from '../../../../slices/stockDetailSlices';
import {useReactToPrint} from 'react-to-print';
import { 
    TextField,
    Grid,
    Button,
    Box,
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Chip,
    Container,
    } from '@mui/material';
    import Paper from '@mui/material/Paper';
import SubCard from 'ui-component/cards/SubCard';
// import { maxWidth } from '@mui/system';

const Form = () => {
    const dispatch=useDispatch();

    const initialProductState = {
        dateFrom: "",
        dateTo: ""
      };
      const [stock, setStock] = useState(initialProductState);
      const [submitted, setSubmitted] = useState(false);
      const stocks = useSelector((state) => state.stockDetailReducer.stockDataPrint)
      const warna = {
        color:'black'
    }
      const handleInputChange = event => {
        const { name, value } = event.target;
        setStock({ ...stock, [name]: value });
        setSubmitted(true);
      };
      const compRef = useRef();
      const handlePrint = useReactToPrint( {
          
          content: () => compRef.current,
          
      });  
  
      useEffect(() => {
          getAllstockDetail();
      }, [dispatch,submitted]);
  
      const getAllstockDetail = () => {
          const param = {
            dateFrom: stock.dateFrom,
            dateTo: stock.dateTo
          };
          dispatch(getPrintStockDetail(param));
      };
      
      const table= () =>{
        
      } 
    
    return ( 
        <Grid item >
            <SubCard >
                <form >
                <Grid 
                    container
                    spacing={3}
                    direction="row"
                    justifyContent="space-between"
                    alignItems="flex-end"
                >
                    
                    <Grid 
                    item 
                    md={4}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Tanggal awal"
                            name="dateFrom"
                            type="date"
                            onChange={handleInputChange}
                            value={stock.dateFrom || ''}
                            required
                            variant="outlined"
                            InputLabelProps={{ shrink: true}}
                        />
                    </Grid>
                    <Grid 
                    item 
                    md={4}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Tanggal akhir"
                            name="dateTo"
                            type="date"
                            onChange={handleInputChange}
                            value={stock.dateTo || ''}
                            required
                            variant="outlined"
                            InputLabelProps={{ shrink: true}}
                        />
                    </Grid>
                    <Grid
                    item
                    container 
                    justifyContent="flex-end"
                    direction="row"
                    
                    >
                        <Button 
                        sx={{ mr: 3,mb:2 }}
                        color="secondary" 
                        variant="contained"
                        onClick={getAllstockDetail}
                        >Cari data</Button>
                        <Button 
                        sx={{ ml: 3, mb:2 }}
                        
                        color="primary" 
                        variant="contained"
                        onClick={handlePrint}
                        >Cetak data</Button>
                    </Grid>
                    
                    
                        
                </Grid>
                </form>

                <Container maxWidth={false}>
                    <Box  ref={compRef}>
                        <Grid>
                            <SubCard>
                                <TableContainer component={Paper}>
                                    <Table size="small" aria-label="a dense table">
                                        <TableHead>
                                            <TableRow>
                                                {/* <TableCell sx={warna}>
                ID
                </TableCell> */}
                                                <TableCell sx={warna}>
                                                    ID Stok
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    ID Produk
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Nama Produk
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Batch
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Sumber Batch
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Issued
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Current
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Balance
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Status
                                                </TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {stocks?.map((stock) => (
                                                <TableRow
                                                    key={stock.id}
                                                >
                                                    {/* <TableCell sx={warna}>
                        {stock.id}
                    </TableCell> */}
                                                    <TableCell sx={warna}>
                                                        {stock.stock_id}
                                                    </TableCell>
                                                    <TableCell sx={warna}>
                                                        {stock.product_id}
                                                    </TableCell>
                                                    <TableCell sx={warna}>
                                                        {stock.name}
                                                    </TableCell>
                                                    <TableCell
                                                        sx={warna}>
                                                        {stock.batch}
                                                    </TableCell>
                                                    <TableCell sx={warna}>
                                                        {stock.batch_from}
                                                    </TableCell>
                                                    <TableCell sx={warna}>
                                                        {stock.is_ussed}
                                                    </TableCell>
                                                    <TableCell
                                                        sx={warna}>
                                                        {stock.current}
                                                    </TableCell>
                                                    <TableCell sx={warna}>
                                                        {stock.balance}
                                                    </TableCell>
                                                    <TableCell>
                                                        <Chip sx={{ color: '#ffffff' }} label={stock.status} color={stock.status === 'IN' ? 'success' : stock.status === 'OUT' ? 'error' : 'primary'} />
                                                    </TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>

                                    </Table>
                                </TableContainer>
                            </SubCard>
                        </Grid>
                    </Box>
                </Container>
            </SubCard>
        </Grid >
     );
}
 
export default Form;