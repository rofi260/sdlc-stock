import React, { useEffect, useRef, useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Box,
    Card,
    CardContent,
    TextField,
    InputAdornment,
    SvgIcon,
    Grid,Button, Chip, Pagination, Tooltip,
    } from '@mui/material';

import SubCard from 'ui-component/cards/SubCard';
// import icon
import {  useTheme } from '@mui/material/styles';
import { useDispatch, useSelector } from 'react-redux';
import { getSearchStockDetail } from '../../../slices/stockDetailSlices';
import { Search } from '@mui/icons-material';
import {useReactToPrint} from 'react-to-print';

// import Form print PDF
import Form from './exportPDF/Form';
import PopUp from './exportPDF/PopUp';

// declare variable



const warna = {
    color:'black'
}





const LogTable = () => {
    const dispatch = useDispatch();
    const stocks = useSelector((state) => state.stockDetailReducer.stockSearchDetails)
    const currpage = useSelector((state) => state.stockDetailReducer.currentPage)
    const count = useSelector((state) => state.stockDetailReducer.totalSearchPages)
    const [page, setPage] = useState(1);
    const [id, setId] = useState(0);
    const [search, setSearch] = useState("");
    const theme = useTheme();
    const [openPopup,setOpenPopup] = React.useState(false)

    const handleChangePage = ( event, newPage) => {
		setPage(newPage);
	};
    const handleSearch = (event) => {
		event.preventDefault();
		setSearch(event.target.searchForm.value);
        setPage(1);
	};

    const compRef = useRef();
    const handlePrint = useReactToPrint( {
        content: () => compRef.current,
    });  

    useEffect(() => {
        getAllstockDetail();
    }, [dispatch,page, count,search]);

    const getAllstockDetail = () => {
		const param = {
			page: page-1,
            search: search
		};
		dispatch(getSearchStockDetail(param));
	};


    return ( 
        <><Box>
            {/* Search component */}
            <Box sx={{ mt: 3 }}>
                <Card>
                    <CardContent>
                        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                            <Box
                                component={'form'}
                                onSubmit={handleSearch}
                                sx={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    width: { sm: 400 },
                                }}
                            >
                                <TextField
                                    fullWidth
                                    InputProps={{
                                        endAdornment: (
                                            <Tooltip title='Cari Data' placement='bottom-start'>
                                                <Button
                                                    type='submit'
                                                    variant='contained'
                                                    size='small'
                                                    color='secondary'
                                                >
                                                    <Search color='gray' />
                                                </Button>
                                            </Tooltip>
                                        ),
                                    }}
                                    label="Pencarian Data"
                                    name='searchForm'
                                    placeholder="ID Produk, Stok atau Nama Produk"
                                    variant="outlined" />
                            </Box>
                            <Box sx={{ m: 1 }}>
                                <Button
                                    // onClick={handlePrint}
                                    onClick= {() => setOpenPopup(true)}
                                    color="primary"
                                    variant="contained"
                                    sx={{ backgroundColor: theme.palette.primary.main, color: '#fff' }}
                                >
                                    Log Produk
                                </Button>
                            </Box>
                        </Box>
                    </CardContent>
                </Card>
            </Box>
            <Box sx={{ mt: 3 }} ref={compRef}>
                <Grid>
                    <SubCard>
                        <TableContainer component={Paper}>
                            <Table size="small" aria-label="a dense table">
                                <TableHead>
                                    <TableRow>
                                        {/* <TableCell sx={warna}>
        ID
    </TableCell> */}
                                        <TableCell sx={warna}>
                                            ID Stok
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            ID Produk
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Nama Produk
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Batch
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Sumber Batch
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Issued
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Current
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Balance
                                        </TableCell>
                                        <TableCell sx={warna}>
                                            Status
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {stocks?.map((stock) => (
                                        <TableRow
                                            key={stock.id}
                                        >
                                            {/* <TableCell sx={warna}>
                {stock.id}
            </TableCell> */}
                                            <TableCell sx={warna}>
                                                {stock.stock_id}
                                            </TableCell>
                                            <TableCell sx={warna}>
                                                {stock.product_id}
                                            </TableCell>
                                            <TableCell sx={warna}>
                                                {stock.name}
                                            </TableCell>
                                            <TableCell
                                                sx={warna}>
                                                {stock.batch}
                                            </TableCell>
                                            <TableCell sx={warna}>
                                                {stock.batch_from}
                                            </TableCell>
                                            <TableCell sx={warna}>
                                                {stock.is_ussed}
                                            </TableCell>
                                            <TableCell
                                                sx={warna}>
                                                {stock.current}
                                            </TableCell>
                                            <TableCell sx={warna}>
                                                {stock.balance}
                                            </TableCell>
                                            <TableCell>
                                                <Chip sx={{ color: '#ffffff' }} label={stock.status} color={stock.status === 'IN' ? 'success' : stock.status === 'OUT' ? 'error' : 'primary'} />
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>

                            </Table>
                            <Pagination
                                sx={{ mt: 2 }}
                                color="primary"
                                count={count}
                                page={page}
                                variant="outlined"
                                size="small"
                                onChange={handleChangePage}
                                shape="rounded" />
                        </TableContainer>
                    </SubCard>
                </Grid>
            </Box>
        </Box>
            {/* // modal dialog print pdf */}
            <PopUp
                title="Cetak ke PDF"
                openPopup={openPopup}
                setOpenPopup={setOpenPopup}
            >
                <Form />
            </PopUp>
            </>
        
     );
}
 
export default LogTable;