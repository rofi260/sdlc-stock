import { Box, Container } from "@mui/material";
import LogHeader from "./LogHeader";
import LogTable from "./LogTable";

const Log = () => {
    return ( 
        <Box
        component="main"
        sx={{
            flexGrow: 1,
        }}
        >
            <Container maxWidth={false}>
                <LogHeader/>
                <Box sx={{ mt: 3 }}>
                    <Box>
                        <LogTable/>
                    </Box>
                </Box>
                
            </Container>
        </Box>
     );
}
 
export default Log;