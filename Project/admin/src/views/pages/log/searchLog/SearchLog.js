import { Box, Container } from "@mui/material";
import SearchLogHeader from "./SearchLogHeader";
import SearchLogTable from "./SearchLogTable";

const SearchLog = () => {
    return ( 
        <Box
        component="main"
        sx={{
            flexGrow: 1,
        }}
        >
            <Container maxWidth={false}>
                <SearchLogHeader/>
                <Box sx={{ mt: 3 }}>
                    <Box>
                        <SearchLogTable/>
                    </Box>
                </Box>
                
            </Container>
        </Box>
     );
}
 
export default SearchLog;