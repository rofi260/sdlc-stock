import { Box, Button, Typography } from "@mui/material";
import {  useTheme } from '@mui/material/styles';


const SearchLogHeader = () => {
    const theme = useTheme();
    return ( 
        <Box>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    flexWrap: 'wrap',
                    m: -1
                }}
            >
                <Typography
                    sx={{
                        m: 1
                    }}
                    variant="h2"
                >
                    Detail Log Produk
                </Typography>
                <Box sx={{ m: 1  }}>
                    <Button 
                        color="primary"
                        variant="contained"
                        sx={{backgroundColor:theme.palette.primary.main, color:'#fff'}}
                    >
                        Cetak daftar Log Produk
                    </Button >
                </Box>
            </Box>
        </Box>
     );
}
 
export default SearchLogHeader;