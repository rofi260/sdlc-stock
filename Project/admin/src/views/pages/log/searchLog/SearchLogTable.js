import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Box,
    TextField,
    Grid,Button,Chip
    } from '@mui/material';
// import MainCard from 'ui-component/cards/MainCard';
import { gridSpacing } from 'store/constant';
// import icon
import {  useTheme } from '@mui/material/styles';
import MainCard from 'ui-component/cards/MainCard';
// import { maxWidth } from '@mui/system';



// declare variable

function detailData( id,idStock, idProduct,batch,issued,current,balance,status) {
    return { id,idStock, idProduct,batch,issued,current,balance,status };
  }


const warna = {
    color:'black'
}

const detail = [
    detailData(1,'STOK0001','PROD001', 'BATCH001','100','0','100','BEGIN'),
    detailData(2,'STOK0002','PROD002', 'BATCH001','100','0','100','IN'),
    detailData(3,'STOK0003','PROD003', 'BATCH001','100','0','100','IN'),
    detailData(4,'STOK0004','PROD004', 'BATCH001','100','0','100','OUT'),
    detailData(5,'STOK0005','PROD005', 'BATCH001','100','0','100','BEGIN'),
    detailData(6,'STOK0006','PROD006', 'BATCH001','100','0','100','IN'),
    detailData(7,'STOK0007','PROD007', 'BATCH001','100','0','100','OUT'),
    detailData(8,'STOK0008','PROD008', 'BATCH001','100','0','100','BEGIN'),
    detailData(9,'STOK0009','PROD008', 'BATCH001','100','0','100','IN'),
    detailData(10,'STOK0010','PROD001', 'BATCH001','100','0','100','OUT'),
];




const SearchLogTable = () => {
    const theme = useTheme();
    return ( 
        <Box>
            <Box sx={{ mt: 3 }}>
                <Grid  item xs={12}>
                    <Grid container spacing={gridSpacing}>
                        <Grid item lg={4} md={4} sm={12} xs={12}>
                            <MainCard title="Transaction Total">
                                <Grid 
                                    container
                                    spacing={2}
                                    direction="row"
                                    justifyContent='center'
                                >
                                    <Grid 
                                    
                                    item lg={6} md={6} sm={12} xs={12}>
                                        <TextField
                                            fullWidth
                                            label="Tanggal awal"
                                            name="startDtae"
                                            // onChange={handleChange}
                                            // value={values.totalProduct}
                                            type="date"
                                            InputLabelProps={{ shrink: true,  style: { fontWeight: 700} }}
                                        />
                                    </Grid>
                                    <Grid 
                                    item lg={6} md={6} sm={12} xs={12}>
                                        <TextField
                                        
                                            fullWidth
                                            label="Tanggal akhir"
                                            name="endDate"
                                            // onChange={handleChange}
                                            // value={values.totalQuantityProduct}
                                            type="date"
                                            InputLabelProps={{ shrink: true,  style: { fontWeight: 500} }}
                                        />
                                    </Grid>
                                    <Grid 
                                    item lg={8} md={8} sm={8} xs={8}  align="center">
                                        <TextField
                                        fullWidth
                                        label="ID Produk"
                                        name="productID"
                                        // onChange={handleChange}
                                        // value={values.totalProductAmount}
                                        variant="outlined"
                                        >

                                        </TextField>
                                    </Grid>
                                    <Grid item lg={12} md={12} sm={12} xs={12} align="center">
                                        <Button 
                                            color="success"
                                            variant="contained"
                                            sx={{backgroundColor:theme.palette.success.dark, color:'#fff'}}
                                        >
                                            Cari daftar Log Produk
                                        </Button >
                                    </Grid>
                                </Grid>
                            </MainCard>
                        </Grid>
                        <Grid item lg={8} md={8} sm={12} xs={12}>
                            <MainCard title="Log Produk">
                                <TableContainer  component={Paper}>
                                    <Table  size="small" aria-label="a dense table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell sx={warna}>
                                                    ID
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    ID Stok
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    ID Produk
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Batch
                                                </TableCell>
                                                <TableCell  sx={warna}>
                                                    Issued
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Current
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Balance
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Status
                                                </TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                        {detail.map((detail)=>(
                                                <TableRow
                                                key={detail.id}
                                                >
                                                    <TableCell sx={warna}>
                                                        {detail.id}
                                                    </TableCell>
                                                    <TableCell sx={warna}>
                                                        {detail.idStock}
                                                    </TableCell>
                                                    <TableCell sx={warna}>
                                                        {detail.idProduct}
                                                    </TableCell>
                                                    <TableCell  
                                                    sx={warna}>
                                                        {detail.batch}
                                                    </TableCell>
                                                    <TableCell  sx={warna}>
                                                        {detail.issued}
                                                    </TableCell>
                                                    <TableCell  
                                                    sx={warna}>
                                                        {detail.current}
                                                    </TableCell>
                                                    <TableCell  sx={warna}>
                                                        {detail.balance}
                                                    </TableCell>
                                                    <TableCell  >
                                                            <Chip sx={{color:'#ffffff'}}  label={detail.status} color={detail.status === 'IN'? 'success' : detail.status === 'OUT' ? 'error' : 'primary'} />
                                                    </TableCell>
                                                </TableRow>
                                                ))}
                                            </TableBody>
                                    </Table>
                                </TableContainer>
                            </MainCard>
                        </Grid >
                    </Grid>
                </Grid>
            
            </Box>
        </Box>
        
     );
}
 
export default SearchLogTable;