// material-ui
import { Box,
    // Button,
     Typography } from '@mui/material';
    

// project imports


const ProductHeader= () => {
    return ( 
        <Box>
            <Box
                sx={{
                    alignItems: 'center',
                    display: 'flex',
                    justifyContent: 'space-between',
                    flexWrap: 'wrap',
                    m: -1
                }}
            >
                <Typography
                    
                    variant="h2"
                >
                    Master Produk
                </Typography>
            </Box>
        </Box>
     );
}
 
export default ProductHeader;