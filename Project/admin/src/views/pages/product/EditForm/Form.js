import  React, {useEffect, useState} from 'react';

import { useDispatch } from 'react-redux';
import { updateProduct} from '../../../../slices/productSlices';
import productService from '../../../../services/productService';

import { 
    TextField,
    Grid,
    Button,
    } from '@mui/material';

import SubCard from 'ui-component/cards/SubCard';
// import { maxWidth } from '@mui/system';

const Form = (props) => {
    
    const initialProductState = {
        id: null,
        name: "",
        price: "",
        expDate: ""
    };
    const getProducts = id => {
        productService.get(id)
          .then(response => {
            setCurrentProduct(response.data);
          })
          .catch(e => {
            console.log(e);
          });
      };
      useEffect(() => {
        getProducts(props.data.id);
      }, [props.data.id]);
    const [currentProduct, setCurrentProduct] = useState(initialProductState);
    const [message, setMessage] = useState("");
    const dispatch=useDispatch();
    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentProduct({ ...currentProduct, [name]: value });
    };


    const updateData = () => {
        
        dispatch(updateProduct({ id: props.data.id, data: currentProduct }))
        
          .unwrap()
          .then(response => {
            console.log(response);
            setMessage("Produk Behasil di Ubah!");
          })
          .catch(e => {
            console.log(e);
          });
      };


    return ( 
        <Grid item >
            <SubCard >
                <form >
                <Grid 
                    container
                    spacing={3}
                    direction="row"
                    justifyContent="space-between"
                    alignItems="flex-end"
                >
                    <Grid 
                    
                    item 
                    md={4}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Nama Produk"
                            name="name"
                            onChange={handleInputChange}
                            value={currentProduct.name}
                            required
                            variant="outlined"
                        />
                    </Grid>
                    <Grid 
                    item 
                    md={4}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Harga Produk"
                            name="price"
                            onChange={handleInputChange}
                            value={currentProduct.price}
                            required
                            variant="outlined"
                        />
                    </Grid>
                    <Grid 
                    item 
                    md={4}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Tanggal kadaluarsa"
                            name="expDate"
                            type="date"
                            onChange={handleInputChange}
                            value={currentProduct.expDate}
                            required
                            variant="outlined"
                            InputLabelProps={{ shrink: true}}
                        />
                    </Grid>
                    <Grid
                    item
                    container 
                    justifyContent="flex-end"
                    >
                        <Button 
                        type="submit" 
                        color="info" 
                        variant="contained"
                        onClick={updateData}
                        >
                            Ubah</Button>
                            <p>{message}</p>
                    </Grid>
                </Grid>
                </form>
            </SubCard>
        </Grid >
     );
}
 
export default Form;