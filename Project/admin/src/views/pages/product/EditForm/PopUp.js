import { Dialog, DialogContent, DialogTitle, Typography,IconButton,  } from '@mui/material'
import { makeStyles } from '@mui/styles';
import React from 'react'
import CloseIcon from '@mui/icons-material/Close';
// import {  useTheme } from '@mui/material/styles';

const useStyles = makeStyles(theme => ({
    dialogWrapper: {
        padding: theme.spacing(2),
        position: 'absolute',
        top: theme.spacing(5),
        
    },
    dialogTitle: {
        paddingRight: '0px'
    }
}))

export default function PopUp(props) {
    const { title, children, openEditPopup, setOpenEditPopup } = props;
    const classes = useStyles();
    // const theme = useTheme();
   

  return (
      
    <Dialog open={openEditPopup} maxWidth="md" classes={{ paper: classes.dialogWrapper }}>
        <DialogTitle>
            <div style={{ display: 'flex' }}>
                <Typography variant="h3" component="div" style={{ flexGrow: 1 }}>
                    {title}
                </Typography>
                <IconButton 
                    color="inherit"
                    size="large"
                   
                    onClick={()=>{setOpenEditPopup(false)}}>
                    <CloseIcon />
                </IconButton >
            </div>
        </DialogTitle>

        <DialogContent >
            {children }
        </DialogContent>
    </Dialog>
  )
}
