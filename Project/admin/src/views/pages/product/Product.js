// material-ui
import { Box,
    Container,} from '@mui/material';
    

// project imports
// import MainCard from 'ui-component/cards/MainCard';

import ProductHeader from './ProductHeader';
import ProductTable from './ProductTable';

const Product= () => {
    return ( 
        <Box
        component="main"
        sx={{
            flexGrow: 1,
        }}
        >
            <Container maxWidth={false}>
                <ProductHeader/>
                <Box sx={{ mt: 3 }}>
                    <Box>
                        <ProductTable/>
                    </Box>
                </Box>
                
            </Container>
        </Box>
     );
}
 
export default Product;