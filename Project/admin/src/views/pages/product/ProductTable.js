import React, {useEffect, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux'; 
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Box,
    Card,
    CardContent,
    TextField,
    InputAdornment,
    SvgIcon,
    Grid,Button,IconButton, Tooltip, Pagination, Typography 
    } from '@mui/material';
// import MainCard from 'ui-component/cards/MainCard';
import { gridSpacing } from 'store/constant';
import SubCard from 'ui-component/cards/SubCard';
// import icon
import {  useTheme } from '@mui/material/styles';
import { IconEdit,IconEye,IconTrash} from "@tabler/icons";
// import { maxWidth } from '@mui/system';


// import Form add Product
import Popup from './Form/PopUp';
import Form from './Form/Form';
// import Form edit Product
import EditPopup from './EditForm/PopUp';
import EditForm from './EditForm/Form';
// import getProduct data
import { getProducts,deleteProduct } from '../../../slices/productSlices';

import { Navigate } from 'react-router';

// import APi




const warna = {
    color:'black'
}


const ProductTable = () => {
    const dispatch = useDispatch();
    const products = useSelector((state) => state.productReducer.product)
    const currpage = useSelector((state) => state.productReducer.currentPage)
    const count = useSelector((state) => state.productReducer.totalPages)
    const [page, setPage] = React.useState(1);
    const [id, setId] = React.useState(0);
    const [nameProd, setNameProd] = React.useState(0);
    const [priceProd, setPriceProd] = React.useState(0);
    const [expDate, setExpDate] = React.useState(0);
    // const [isEdit, setIsEdit] = React.useState(false); off karena masih fail Dialog untuk add dan edit
    const [data, setData] = React.useState();
    const handleChangePage = ( event, newPage) => {
		setPage(newPage);
	};


    useEffect(() => {
        getAllproduct();
    }, [dispatch,page, count]);

    const getAllproduct = () => {
		const param = {
			page: page-1,
		};
		dispatch(getProducts(param));
	};
    
    const edit = (id,nameProd,priceProd,expDate) => {
        // setIsEdit(true);
        setData({id,nameProd,priceProd,expDate});
        setOpenEditPopup(true)
    }
    // fill detail table
    const detailTable = (product) => {
        setId(product.id);
        setNameProd(product.name);
        setPriceProd(product.price);
        setExpDate(product.expDate);
    }
    const removeProduct = (id) => {
        dispatch(deleteProduct({ id: id }))
          .unwrap()
          .then(() => {
           Navigate("/product");
          })
          .catch(e => {
            console.log(e);
          });
      };
    
    const [openPopup,setOpenPopup] = React.useState(false)
    const [openEditPopup,setOpenEditPopup] = React.useState(false)
    const theme = useTheme();
    return ( 
        <>
        <Box>
            {/* Search component */}
            <Box sx={{ mt: 3 }}>
                <Card>
                    <CardContent>
                        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                            <Box sx={{ maxWidth: 500 }}>
                                {/* <TextField
                                    fullWidth
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <SvgIcon
                                                    color="action"
                                                    fontSize="small"
                                                >
                                                    <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-search" width="25" height="25" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#597e8d" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                        <circle cx="10" cy="10" r="7" />
                                                        <line x1="21" y1="21" x2="15" y2="15" />
                                                    </svg>
                                                </SvgIcon>
                                            </InputAdornment>
                                        )
                                    }}
                                    placeholder="Search Produk"
                                    variant="outlined" /> */}
                            </Box>
                            <Box sx={{ m: 1 }}>
                                <Button
                                    color="secondary"
                                    variant="contained"
                                    sx={{ backgroundColor: theme.palette.secondary.main, color: '#fff' }}
                                    onClick= {() => setOpenPopup(true)}
                                >
                                    Tambah Produk
                                </Button>
                            </Box>
                        </Box>
                    </CardContent>
                </Card>
            </Box>
            <Box sx={{ mt: 3 }}>
                <Grid item xs={12}>
                    <Grid container spacing={gridSpacing}>
                        <Grid item lg={3} md={6} sm={6} xs={12}>
                            <SubCard title="List Produk ">
                                <TableContainer component={Paper}>
                                    <Table size="small">
                                        <TableBody>
                                            {products?.map((product) => (
                                                <TableRow
                                                    key={product.id}
                                                >
                                                    <TableCell sx={warna} component="th" scope="row">
                                                        <b>{product.name}</b> <br />
                                                        Product ID: {product.id}
                                                    </TableCell>
                                                    <TableCell>
                                                        <Tooltip describeChild title="Tekan untuk meilhat detail" arrow>
                                                            <IconButton onClick={() => detailTable(product)} sx={{ color: theme.palette.primary.main }}>
                                                                <IconEye />
                                                            </IconButton>
                                                        </Tooltip>
                                                    </TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                                <Pagination
                                sx={{ mt: 2 }}
                                    color="primary"
                                    count={count}
                                    page={page}
                                    variant="outlined"
                                    size="small"
                                    onChange={handleChangePage}
                                    shape="rounded"
                                    />
                            </SubCard>
                        </Grid>
                        <Grid item lg={9} md={12} sm={12} xs={12}>
                            <SubCard title="Detail Produk ">
                                <TableContainer component={Paper}>
                                    <Table size="small" aria-label="a dense table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell sx={warna}>
                                                    ID Produk
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Nama Produk
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Harga Produk
                                                </TableCell>
                                                <TableCell sx={warna}>
                                                    Kadaluarsa
                                                </TableCell>
                                                <TableCell align="center" sx={warna}>
                                                    Aksi
                                                </TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {/* {detail.map((detail) => ( */}
                                                <TableRow
                                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                    
                                                >
                                                    <TableCell sx={warna}>
                                                        {id}
                                                    </TableCell>
                                                    <TableCell sx={warna}>
                                                        {nameProd}
                                                    </TableCell>
                                                    <TableCell sx={warna}>
                                                       {priceProd.toLocaleString("id-ID", {style:"currency", currency:"IDR"})}
                                                    </TableCell>
                                                    <TableCell sx={warna}>
                                                        {expDate}
                                                    </TableCell>
                                                    <TableCell align="center" sx={warna}>
                                                        <Tooltip describeChild title="Tekan untuk mengubah data" arrow>
                                                            <IconButton
                                                                onClick= {() => edit(id,nameProd,priceProd,expDate)}
                                                                // onClick= {() => console.log(id)}
                                                                sx={{ color: theme.palette.warning.dark }}
                                                            >
                                                                <IconEdit />
                                                            </IconButton>
                                                        </Tooltip>
                                                    </TableCell>
                                                </TableRow>
                                            {/* ))} */}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </SubCard>
                        </Grid>
                    </Grid>
                </Grid>

            </Box>
        </Box>
        {/* Modal add Product */}
        <Popup 
        title = "Form Tambah Produk"
        openPopup= {openPopup}
        setOpenPopup = {setOpenPopup}
        >
            <Form/>
        </Popup>
        {/* Modal Edit Product */}
        <EditPopup 
        title = "Form Edit Produk"
        openEditPopup= {openEditPopup}
        setOpenEditPopup = {setOpenEditPopup}
        >
            <EditForm data={data}/>
        </EditPopup>
        </>
        
     );
}
 
export default ProductTable;