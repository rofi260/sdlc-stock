import  React, {useState} from 'react';

import { useDispatch } from 'react-redux';
import { addProducts} from '../../../../slices/productSlices';

import { 
    TextField,
    Grid,
    Button,
    } from '@mui/material';

import SubCard from 'ui-component/cards/SubCard';
// import { maxWidth } from '@mui/system';

const Form = () => {
    const dispatch=useDispatch();

    const initialProductState = {
        
        name: "",
        price: "",
        expDate: ""
      };
      const [product, setProduct] = useState(initialProductState);
      const [submitted, setSubmitted] = useState(false);


      const handleInputChange = event => {
        const { name, value } = event.target;
        setProduct({ ...product, [name]: value });
      };
      const saveProduct = () => {
        const { name, price, expDate } = product;
        dispatch(addProducts({ name, price, expDate }))
          .unwrap()
          .then(data => {
            console.log(data);
            setProduct({
                name: data.name,
              price: data.price,
              expDate: data.expDate
            });
            setSubmitted(true);
          })
          .catch(e => {
            console.log(e);
          });
      };
      
    
    return ( 
        <Grid item >
            <SubCard >
                <form >
                <Grid 
                    container
                    spacing={3}
                    direction="row"
                    justifyContent="space-between"
                    alignItems="flex-end"
                >
                    <Grid 
                    
                    item 
                    md={4}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Nama Produk"
                            name="name"
                            onChange={handleInputChange}
                            value={product.name || ''}
                            required
                            variant="outlined"
                        />
                    </Grid>
                    <Grid 
                    item 
                    md={4}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Harga Produk"
                            name="price"
                            onChange={handleInputChange}
                            value={product.price || ''}
                            required
                            variant="outlined"
                        />
                    </Grid>
                    <Grid 
                    item 
                    md={4}
                    sm={6}
                    xs={12}>
                        <TextField
                            fullWidth
                            label="Tanggal kadaluarsa"
                            name="expDate"
                            type="date"
                            onChange={handleInputChange}
                            value={product.expDate || ''}
                            required
                            variant="outlined"
                            InputLabelProps={{ shrink: true}}
                        />
                    </Grid>
                    <Grid
                    item
                    container 
                    justifyContent="flex-end"
                    >
                        <Button 
                        type="submit" 
                        color="info" 
                        variant="contained"
                        onClick={saveProduct}
                        >Tambahkan</Button>
                    </Grid>
                </Grid>
                </form>
            </SubCard>
        </Grid >
     );
}
 
export default Form;