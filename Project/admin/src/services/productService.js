import httpCommon from "http-common";

const get = id => {
    return httpCommon.get(`/product/${id}`);
  };
  const create = data => {
    return httpCommon.post("/product", data);
  };
  const update = (id, data) => {
    return httpCommon.put(`/product/${id}`, data);
  };
  const remove = id => {
    return httpCommon.delete(`/product/${id}`);
  };
  const getProduct = () => {
    return httpCommon.get(`/product/data`);
  };

  const productService = {
    get,
    create,
    update,
    remove,
    getProduct,
  };
  export default productService;
  
