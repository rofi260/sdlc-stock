import httpCommon from "http-common";

  const get = id => {
    return httpCommon.get(`/stockdetail/${id}`);
  };
  const create = data => {
    return httpCommon.post(`/stockdetail`, data);
  };
  const getDataStock = id => {
    return httpCommon.get(`/stockdetail/stock/${id}`);
  };
  const getLogStock = id => {
    return httpCommon.get(`/stockdetail/${id}`);
  };
  const takeLogStock = data => {
    return httpCommon.post(`/stockdetail/checkout`,data);
  };
  const deleteLogStock=(id) =>{
    return httpCommon.get(`/stockdetail/delete?id=`+id);
  };
  

  const stockDetailService = {
    get,
    create,
    getDataStock,
    getLogStock,
    takeLogStock,
    deleteLogStock,
  };
  export default stockDetailService;
  
