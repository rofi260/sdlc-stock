import httpCommon from "http-common";

const get = id => {
    return httpCommon.get(`/stock/${id}`);
  };
  const create = data => {
    return httpCommon.post("/stock", data);
  };
  const update = (id, data) => {
    return httpCommon.put(`/stock/${id}`, data);
  };
  const remove = id => {
    return httpCommon.delete(`/stock/${id}`);
  };

  const stockService = {
    get,
    create,
    update,
    remove,
  };
  export default stockService;
  
