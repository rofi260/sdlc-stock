import { lazy } from 'react';

// project imports
import MainLayout from 'layout/MainLayout';
import Loadable from 'ui-component/Loadable';

// dashboard routing
const DashboardDefault = Loadable(lazy(() => import('views/dashboard/Default')));

// utilities routing
const UtilsTypography = Loadable(lazy(() => import('views/utilities/Typography')));
const UtilsColor = Loadable(lazy(() => import('views/utilities/Color')));
const UtilsShadow = Loadable(lazy(() => import('views/utilities/Shadow')));
const UtilsMaterialIcons = Loadable(lazy(() => import('views/utilities/MaterialIcons')));
const UtilsTablerIcons = Loadable(lazy(() => import('views/utilities/TablerIcons')));

// product page routing
const Product = Loadable(lazy(() => import('views/pages/product/Product')));
// Stock page routing
const Stock = Loadable(lazy(() => import('views/pages/stock/Stock')));
const StockDetail = Loadable(lazy(() => import('views/pages/stock/detail/StockDetail')));
// Log page routing
const Log = Loadable(lazy(() => import('views/pages/log/Log')));
const LogDetail = Loadable(lazy(() => import('views/pages/log/searchLog/SearchLog')));
// sample page routing
const SamplePage = Loadable(lazy(() => import('views/sample-page')));

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
    path: '/',
    element: <MainLayout />,
    children: [
        {
            path: '/',
            element: <DashboardDefault />
        },
        {
            path: '/dashboard',
            element: <DashboardDefault />
        },
        {
            path: '/utils/util-typography',
            element: <UtilsTypography />
        },
        {
            path: '/utils/util-color',
            element: <UtilsColor />
        },
        {
            path: '/utils/util-shadow',
            element: <UtilsShadow />
        },
        {
            path: '/icons/tabler-icons',
            element: <UtilsTablerIcons />
        },
        {
            path: '/icons/material-icons',
            element: <UtilsMaterialIcons />
        },
        {
            path: '/sample-page',
            element: <SamplePage />
        },
        {
            path: '/product',
            element: <Product />
        },
        {
            path: '/stok',
            element: <Stock/>
        },
        {
            path: `/stok/detail/:id`,
            element: <StockDetail/>
        },
        {
            path: '/log',
            element: <Log/>
        },
        {
            path: '/log/detail',
            element: <LogDetail/>
        }
    ]
};

export default MainRoutes;
