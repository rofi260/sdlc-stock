package com.nd6.stok.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table (name = "product")
public class Product {
    @Id
    @GenericGenerator(name = "string_Product_Generator", strategy = 
    "com.nd6.stok.models.IdGenerator.ProductIdGenerator")
    @GeneratedValue(generator = "string_Product_Generator")
    String id;
    
    @Column(length = 50)
    @Size(max = 50)
    String name;

    @NotNull
    Date expDate;

    @Column(length = 50)
    int quantity;

    @Column(length = 50)
    int price;

    public Product(@Size(max = 50) String name, @NotNull Date expDate,  int quantity,
             int price) {
        this.name = name;
        this.expDate = expDate;
        this.quantity = quantity;
        this.price = price;
    }
}
