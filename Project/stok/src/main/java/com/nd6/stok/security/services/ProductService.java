package com.nd6.stok.security.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nd6.stok.models.Product;
import com.nd6.stok.models.DTO.ProductDTO;
import com.nd6.stok.models.DTO.ProductDTO.ProductData;
import com.nd6.stok.payload.response.ResourceNotFoundException;
import com.nd6.stok.repository.ProductRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

@Service
public class ProductService {
    @Autowired
    ProductRepo productRepo;

    private Sort.Direction getSortDirection(String direction) {
        if (direction.equals("asc")) {
          return Sort.Direction.ASC;
        } else if (direction.equals("desc")) {
          return Sort.Direction.DESC;
        }
    
        return Sort.Direction.ASC;
      }

    public ResponseEntity<List<Product>> getAll(String name){
        List<Product> products = new ArrayList<Product>();
    if (name == null)
      productRepo.findAll().forEach(products::add);
    else
    productRepo.findByName(name).forEach(products::add);
    if (products.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    return new ResponseEntity<>(products, HttpStatus.OK);
        // return productRepo.findAll();
    }

    public ResponseEntity< Product> save (Product product){  
        Product _product = productRepo.save(new Product(product.getName(), product.getExpDate(), product.getQuantity(), product.getPrice()));
        return new ResponseEntity<>(_product, HttpStatus.CREATED);
        
    }

    public ResponseEntity<HttpStatus> delete(String id){
        productRepo.deleteById(id);
    
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Product> getById (String id){
        Product product = productRepo.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Not found Product with id = " + id));
        return new ResponseEntity<>(product, HttpStatus.OK);
       
    }

    public ResponseEntity<Product> updateById(String id,Product product){
        Product _product = productRepo.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Not found Product with id = " + id));
    _product.setName(product.getName());
    _product.setPrice(product.getPrice());
    _product.setQuantity(product.getQuantity());
    _product.setExpDate(product.getExpDate());
    
    return new ResponseEntity<>(productRepo.save(_product), HttpStatus.OK);
    }

    // get data with pagination and sorting
    public ResponseEntity<Map<String, Object>> findAll(String id,int page, int size, String[] sort) {
        try {
            List<Order> orders = new ArrayList<Order>();
      
            if (sort[0].contains(",")) {
              // sortOrder="field, direction"
              for (String sortOrder : sort) {
                String[] _sort = sortOrder.split(",");
                orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
              }
            } else {
              // sort=[field, direction]
              orders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }
      
            List<Product> products = new ArrayList<Product>();
            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));
      
            Page<Product> prodData;
            if (id == null)
              prodData = productRepo.findAll(pagingSort);
            else
              prodData = productRepo.findById(id, pagingSort);
      
            products = prodData.getContent();
      
            Map<String, Object> response = new HashMap<>();
            response.put("products", products);
            response.put("currentPage", prodData.getNumber());
            response.put("totalItems", prodData.getTotalElements());
            response.put("totalPages", prodData.getTotalPages());
            response.put("lastPage", prodData.isLast());
            response.put("firstPage", prodData.isFirst());
            // response.put("pagination", prodData);
      
            return new ResponseEntity<>(response, HttpStatus.OK);
          } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
          }
    }

    public ResponseEntity<List<ProductData>> productData() {
      List<ProductData> products = new ArrayList<ProductData>();
      productRepo.findDataProduct().forEach(products::add);
      if (products.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }
      return new ResponseEntity<>(products, HttpStatus.OK);
    }
}
