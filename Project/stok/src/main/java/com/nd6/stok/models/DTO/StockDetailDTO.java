package com.nd6.stok.models.DTO;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class StockDetailDTO {
    int id;
    String batch;
    int price;
    int current;
    int balance;
    int is_ussed;
    String status;
    String batch_from;
    String exp_date;
    String product_id;
    String stock_id;

    public StockDetailDTO(int price, int current, String exp_date, String product_id, String stock_id) {
        this.price = price;
        this.current = current;
        this.exp_date = exp_date;
        this.product_id = product_id;
        this.stock_id = stock_id;
    }

    

    public StockDetailDTO(String batch, int price, int current, int balance, Integer is_ussed, String status,
            String batch_from, String exp_date, String product_id, String stock_id) {
        this.batch = batch;
        this.price = price;
        this.current = current;
        this.balance = balance;
        this.is_ussed = is_ussed;
        this.status = status;
        this.batch_from = batch_from;
        this.exp_date = exp_date;
        this.product_id = product_id;
        this.stock_id = stock_id;
    }

    public interface StockStock{
    Integer getProduct();
    Integer getCurrent();
    Integer getUntung();
    }

    public interface GetStockDetail{
        int getId();
    String getBatch();
    int getPrice();
    int getCurrent();
    int getBalance();
    int getIs_ussed();
    String getStatus();
    String getBatch_from();
    String getExp_date();
    String getProduct_id();
    String getStock_id();
    }

    public interface GetStockDetailProduct{
        int getId();
    String getBatch();
    int getPrice();
    int getCurrent();
    int getBalance();
    int getIs_ussed();
    String getStatus();
    String getBatch_from();
    String getExp_date();
    String getProduct_id();
    String getStock_id();
    String getName();
    }

    public interface GetBatch{
        String getBatch();
    }

    public interface GetCurrent{
        Integer getCurrent();
    }

    public interface GetCurrentProduct{
        Integer getId();
        String getBatch();
        Integer getCurrent();
    }
}
