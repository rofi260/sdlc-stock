package com.nd6.stok.security.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.nd6.stok.models.StockDetail;
import com.nd6.stok.models.DTO.StockDetailDTO;
import com.nd6.stok.models.DTO.StockDetailDTO.GetStockDetailProduct;
import com.nd6.stok.repository.StockDetailRepository;
import com.nd6.stok.payload.response.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class StockDetailService {
    
      @Autowired
      StockDetailRepository stockDetailRepository;

    private Sort.Direction getSortDirection(String direction) {
        if (direction.equals("asc")) {
          return Sort.Direction.ASC;
        } else if (direction.equals("desc")) {
          return Sort.Direction.DESC;
        }
    
        return Sort.Direction.ASC;
    }

    public ResponseEntity<List<StockDetail>> getAll(Integer id){
        List<StockDetail> stockDetail = new ArrayList<StockDetail>();
        if (id == null)
        stockDetailRepository.findAll().forEach(stockDetail::add);
        else
        stockDetailRepository.findById(id);
        if (stockDetail.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(stockDetail, HttpStatus.OK);
    }

    // public StockDetail save (StockDetail stockDetail){
    //     return stockDetailRepository.save(stockDetail);
    // }

    public void delete(int id){
        stockDetailRepository.deleteById(id);;
    }

    public Optional<StockDetail> getById (int id){
        return stockDetailRepository.findById(id);
    }

    public ResponseEntity<Map<String, Object>> getAll(Integer id, int page, int size, String[] sort) {
        try {
            List<Order> orders = new ArrayList<Order>();
      
            if (sort[0].contains(",")) {
              // sortOrder="field, direction"
              for (String sortOrder : sort) {
                String[] _sort = sortOrder.split(",");
                orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
              }
            } else {
              // sort=[field, direction]
              orders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }
      
            List<StockDetailDTO.GetStockDetail> stockDetail = new ArrayList<StockDetailDTO.GetStockDetail>();
            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));
      
            // Page<StockDetail> stockData;
            Page<StockDetailDTO.GetStockDetail> stockDataDTO;
            if (id == null)
            stockDataDTO = stockDetailRepository.findAllStockDetail(pagingSort);
            
            else
            stockDataDTO = stockDetailRepository.findById(id, pagingSort);
      
            stockDetail = stockDataDTO.getContent();
      
            Map<String, Object> response = new HashMap<>();
            response.put("stockDetails", stockDetail);
            response.put("currentPage", stockDataDTO.getNumber());
            response.put("totalItems", stockDataDTO.getTotalElements());
            response.put("totalPages", stockDataDTO.getTotalPages());
            // response.put("pagination", stockData);
      
            return new ResponseEntity<>(response, HttpStatus.OK);
          } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Map<String, Object>> getByIdStock(String id, int page, int size, String[] sort){
        try {
            List<Order> orders = new ArrayList<Order>();
      
            if (sort[0].contains(",")) {
              // sortOrder="field, direction"
              for (String sortOrder : sort) {
                String[] _sort = sortOrder.split(",");
                orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
              }
            } else {
              // sort=[field, direction]
              orders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }
      
            List<StockDetailDTO.GetStockDetail> stockDetail = new ArrayList<StockDetailDTO.GetStockDetail>();
            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));
      
            // Page<StockDetail> stockData;
            Page<StockDetailDTO.GetStockDetail> stockDataDTO;
            if (id == null)
            stockDataDTO = stockDetailRepository.findAllStockDetail(pagingSort);
            
            else
            stockDataDTO = stockDetailRepository.findByIdStock(id, pagingSort);
      
            stockDetail = stockDataDTO.getContent();
      
            Map<String, Object> response = new HashMap<>();
            response.put("stockDetails", stockDetail);
            response.put("currentPage", stockDataDTO.getNumber());
            response.put("totalItems", stockDataDTO.getTotalElements());
            response.put("totalPages", stockDataDTO.getTotalPages());
            // response.put("pagination", stockData);
      
            return new ResponseEntity<>(response, HttpStatus.OK);
          } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
          }
    }
    public StockDetailDTO.StockStock getStockDetailStock(String id){
      
      return stockDetailRepository.findStockDataStock(id);
    }

    public ResponseEntity< Object> save (StockDetailDTO stockDetail){  
         // find ProductId
      StockDetailDTO.GetBatch existProductId = stockDetailRepository.findByIdProduct(stockDetail.getProduct_id());
     
      if (existProductId==null) {
        
        int _stockDetail = stockDetailRepository.saveData(
         "BATCH000",stockDetail.getPrice(),stockDetail.getCurrent(),stockDetail.getCurrent(),0
          ,"BEGIN","",stockDetail.getExp_date(),stockDetail.getProduct_id()
          ,stockDetail.getStock_id());
          if (_stockDetail == 1) {
            return  ResponseEntity.status(HttpStatus.OK)
            .body(Map.of("message", "Stok Baru Berhasil dibuat.", "status",
                         "200"));
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Map.of("message", "Input Gagal!", "status", "400"));
        }
      }else{
        // ambil jumlah current produk
        System.out.println(stockDetail.getProduct_id());
        StockDetailDTO.GetCurrent tempCurrent = stockDetailRepository.findCountCurrent(stockDetail.getProduct_id());
        System.out.println(stockDetail.getCurrent());
        System.out.println(stockDetail.getProduct_id());
        
        int totalCurrent = tempCurrent.getCurrent()+stockDetail.getCurrent();
        System.out.println(totalCurrent);
        String prefix = "BATCH";
        String curentBatch=existProductId.getBatch();
        int convert= Integer.parseInt(curentBatch.replaceAll("[^0-9]", ""));
        int newBatch = convert+1;
        String suffix = String.format("%03d", newBatch);
        String generatedBatch = prefix.concat(suffix);

        int _stockDetail = stockDetailRepository.saveData(
          generatedBatch,stockDetail.getPrice(),stockDetail.getCurrent(),totalCurrent,0
          ,"IN","",stockDetail.getExp_date(),stockDetail.getProduct_id()
          ,stockDetail.getStock_id());
        if (_stockDetail == 1) {
            return  ResponseEntity.status(HttpStatus.OK)
            .body(Map.of("message", "Stok Berhasil ditambahkan.", "status",
                         "200"));
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Map.of("message", "Input Gagal!", "status", "400"));
        }
      }
    }


    public ResponseEntity< Object> takeStock (StockDetailDTO stockDetail){
      
      int currrentStockProduct=0;  
      int currentStockId=0;
      String currentBatchStock=null;
      
      // amount requested product
      int amountRequest = stockDetail.getCurrent();


      // get total current product from DB
      StockDetailDTO.GetCurrent tempCurrent = stockDetailRepository.findCountCurrent(stockDetail.getProduct_id());
      int totalCurrent = tempCurrent.getCurrent();

      if ((amountRequest <= totalCurrent)&&(amountRequest>0)) {
        do {
        // get product by expDate with closest exp date and current not null;
        StockDetailDTO.GetCurrentProduct currentProduct = stockDetailRepository.findProductAvailable(stockDetail.getProduct_id());
        currrentStockProduct = currentProduct.getCurrent();
        currentStockId = currentProduct.getId();
        currentBatchStock = currentProduct.getBatch();
        // count request-current product stock
                
          if (amountRequest>=currrentStockProduct) {
            // System.out.println("counterStock awal saat ini :"+amountRequest);
            amountRequest=amountRequest-currrentStockProduct;
            totalCurrent=totalCurrent-currrentStockProduct;
            
            stockDetailRepository.saveData(
           "",stockDetail.getPrice(),0,totalCurrent,currrentStockProduct
            ,"OUT",currentBatchStock,stockDetail.getExp_date(),stockDetail.getProduct_id()
            ,stockDetail.getStock_id());

            currrentStockProduct=0;
            // update product by id stock
            stockDetailRepository.updateDataProduct(currentStockId, currrentStockProduct);
            // save data checkout
  
            // System.out.println("counterStock setelah di get :"+amountRequest);
          } if ((amountRequest<currrentStockProduct)&&(amountRequest>0)){
            System.out.println("amountRequest saat ini :"+amountRequest);
            int balance=totalCurrent-amountRequest;
            int amountCurrentProduct=currrentStockProduct-amountRequest;
            
            // disini update current produknya menggunakan amountCurrentProduct
  
            // save data checkout
            stockDetailRepository.saveData(
            "",stockDetail.getPrice(),0,balance,amountRequest
            ,"OUT",currentBatchStock,stockDetail.getExp_date(),stockDetail.getProduct_id()
            ,stockDetail.getStock_id());
            
            // update product by id stock
            stockDetailRepository.updateDataProduct(currentStockId, amountCurrentProduct);
            amountRequest=0;
            // amountRequest=0;
            System.out.println("amountRequest saat ini kurang dari stok tersedia:"+amountRequest+" "+amountCurrentProduct);
          
          }
        } while (amountRequest !=0);
        
        return ResponseEntity.status(HttpStatus.OK)
        .body(Map.of("message", "habis sudah request"+amountRequest, "status", "200"));
      } else {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Map.of("message", "lebih gede", "status", "400"));
      }

    }
  
    // delete stock detail product by ID
    public ResponseEntity<Object> deleteStockById (int id){
      int deleted = stockDetailRepository.deleteStockDetail(id);
      if (deleted == 1) {
          return ResponseEntity.status(HttpStatus.OK)
                  .body(Map.of("message", "Stok detail " + "telah dihapus.", "status", "200"));
      } else {
          return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                  .body(Map.of("message", "Data Gagal dihapus!", "status", "400"));
      }

    }

    public ResponseEntity<Map<String, Object>> getAllSearch(Integer id, int page, int size, String[] sort,
        String search) {
        try {
          List<Order> orders = new ArrayList<Order>();
    
          if (sort[0].contains(",")) {
            // sortOrder="field, direction"
            for (String sortOrder : sort) {
              String[] _sort = sortOrder.split(",");
              orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
            }
          } else {
            // sort=[field, direction]
            orders.add(new Order(getSortDirection(sort[1]), sort[0]));
          }
    
          List<StockDetailDTO.GetStockDetailProduct> stockDetail = new ArrayList<StockDetailDTO.GetStockDetailProduct>();
          Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));
    
          // Page<StockDetail> stockData;
          Page<StockDetailDTO.GetStockDetailProduct> stockDataDTO;
          if (search != null)
          stockDataDTO = stockDetailRepository.findStockDetailSearch(pagingSort,search);

          else
          stockDataDTO = stockDetailRepository.findStockDetailJoin(pagingSort);
          
    
          stockDetail = stockDataDTO.getContent();
    
          Map<String, Object> response = new HashMap<>();
          response.put("stockDetails", stockDetail);
          response.put("currentPage", stockDataDTO.getNumber());
          response.put("totalItems", stockDataDTO.getTotalElements());
          response.put("totalPages", stockDataDTO.getTotalPages());
          // response.put("pagination", stockData);
    
          return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
          return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }

    public ResponseEntity<List<GetStockDetailProduct>> printDetailStock(String dateFrom, String dateTo) {
     
        List<GetStockDetailProduct> stockDetail = new ArrayList<GetStockDetailProduct>();
        if (dateFrom == null||dateTo==null)
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        else
        stockDetail=stockDetailRepository.printStockDetailSearch(dateFrom,dateTo);
        
        return new ResponseEntity<>(stockDetail, HttpStatus.OK);
    }

}
