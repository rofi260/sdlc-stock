package com.nd6.stok.models.IdGenerator;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

public class ProductIdGenerator implements IdentifierGenerator {

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        String prefix = "PROD";
        Connection connection = session.connection();
    
        try {
            Statement statement=connection.createStatement();
    
            ResultSet rs=statement.executeQuery("select count(id) as Id from product");
    
            if(rs.next())
            {
                int id=rs.getInt(1)+1;
                String suffix = String.format("%04d", id);
                String generatedId = prefix.concat(suffix);
                return generatedId;
            
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    
        return null;
    }
    
}
