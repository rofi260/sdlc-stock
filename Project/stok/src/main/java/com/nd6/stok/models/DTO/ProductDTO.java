package com.nd6.stok.models.DTO;

import java.sql.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ProductDTO {

    String id;
    String name;
    public ProductDTO(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public interface ProductData {
        String getId();
        String getName();
    }
    public interface Product {
        Integer getProduct();
        Integer getCurrent();
        Integer getUntung();
    }
}


