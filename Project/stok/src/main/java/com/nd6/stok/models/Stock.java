package com.nd6.stok.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Stock {

    @Id
    @GenericGenerator(name = "string_based_custom_sequence", strategy = 
    "com.nd6.stok.models.IdGenerator.StockIdGenerator")
    @GeneratedValue(generator = "string_based_custom_sequence")
    String id;

    @NotNull
    Date stock_date;

    @Column(columnDefinition = "TEXT")
    String description;

    @Column(columnDefinition = "TEXT")
    String status;

    public Stock(@NotNull Date stock_date, String description, String status) {
        this.stock_date = stock_date;
        this.description = description;
        this.status = status;
    }

    public Stock(String description, @NotNull Date stock_date) {
        this.stock_date = stock_date;
        this.description = description;
    }
    
}