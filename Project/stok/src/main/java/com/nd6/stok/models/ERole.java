package com.nd6.stok.models;

public enum ERole {
  ROLE_USER,
  ROLE_ADMIN
}
