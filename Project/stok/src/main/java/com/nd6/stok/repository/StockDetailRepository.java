package com.nd6.stok.repository;


import java.util.List;

import com.nd6.stok.models.StockDetail;
import com.nd6.stok.models.DTO.StockDetailDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface StockDetailRepository extends JpaRepository<StockDetail,Integer>{

    Page<StockDetailDTO.GetStockDetail> findById(Integer id, Pageable pagingSort);

    @Query(nativeQuery = true,value = "SELECT id,batch,exp_date,price,current,balance,batch_from,is_ussed,status,product_id,stock_id FROM stock_detail")
    Page<StockDetailDTO.GetStockDetail>findAllStockDetail(Pageable pagingSort);
    
    // with search and join
    // SELECT `stock_detail`.batch,`stock_detail`.exp_date,`stock_detail`.price,`stock_detail`.current,`stock_detail`.balance,`stock_detail`.batch_from,`stock_detail`.is_ussed,`stock_detail`.status,`product`.`id`,`product`.`name`,`stock_detail`.stock_id FROM `stock_detail` LEFT JOIN `product` on `stock_detail`.`product_id`=`product`.`id` where stock_id LIKE "%?fan%" OR product_id LIKE "%fan%" OR `product`.`name` LIKE "%fan%"
    @Query(nativeQuery = true,value = "SELECT `stock_detail`.id,`stock_detail`.batch,`stock_detail`.exp_date,`stock_detail`.price,"+
    "`stock_detail`.current,`stock_detail`.balance,`stock_detail`.batch_from,`stock_detail`.is_ussed,`stock_detail`.status,"+
    "`stock_detail`.product_id,`stock_detail`.stock_id, `product`.`name`"+
    " FROM `stock_detail` LEFT JOIN `product` on `stock_detail`.`product_id`=`product`.`id`"+
    " where stock_id LIKE %?1% OR product_id LIKE %?1% OR `product`.`name` LIKE %?1%")
    Page<StockDetailDTO.GetStockDetailProduct>findStockDetailSearch(Pageable pagingSort,@Param("id") String id);
    // only join, without search
    @Query(nativeQuery = true,value = "SELECT `stock_detail`.id,`stock_detail`.batch,`stock_detail`.exp_date,`stock_detail`.price,"+
    "`stock_detail`.current,`stock_detail`.balance,`stock_detail`.batch_from,`stock_detail`.is_ussed,`stock_detail`.status,"+
    "`stock_detail`.product_id,`stock_detail`.stock_id, `product`.`name`"+
    " FROM `stock_detail` LEFT JOIN `product` on `stock_detail`.`product_id`=`product`.`id`")
    Page<StockDetailDTO.GetStockDetailProduct>findStockDetailJoin(Pageable pagingSort);

    @Query(value = "SELECT id,batch,exp_date,price,current,balance,batch_from,is_ussed,status,product_id,stock_id FROM `stock_detail` WHERE stock_id=?1", nativeQuery = true)
    Page<StockDetailDTO.GetStockDetail> findByIdStock(String StockId, Pageable pagingSort);
    
    // @Query(value = "SELECT COUNT(DISTINCT(product_id)) as product,SUM(current ) as current,(current*price) as untung FROM `stock_detail` WHERE stock_id=?1 AND status=?2"  , nativeQuery=true)
    // SELECT COUNT(DISTINCT(product_id)) as product,SUM(current ) as current,((SELECT SUM(is_ussed*price) FROM `stock_detail` where status="OUT")-(SELECT SUM(current*price) FROM `stock_detail` where status="IN")) as untung FROM `stock_detail` WHERE stock_id="STOK0001"
    @Query(value = "SELECT COUNT(DISTINCT(product_id)) as product,SUM(current ) as current,((SELECT SUM(is_ussed*price) FROM `stock_detail` where status='OUT')-(SELECT SUM(current*price) FROM `stock_detail` where status='IN')) as untung FROM `stock_detail` WHERE stock_id=?1"  , nativeQuery=true)
    StockDetailDTO.StockStock findStockDataStock(String id);

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO `stock_detail`"+
     "( `balance`, `batch`, `batch_from`, `current`, `exp_date`, `is_ussed`, `price`, `status`, `product_id`, `stock_id`)"+
     "VALUES (:balance ,:batch ,:batch_from ,:current ,:exp_date ,:is_ussed ,:price ,:status ,:product_id ,:stock_id)", nativeQuery=true)
     int saveData(@Param("batch") String batch,@Param("price") int price,@Param("current") int current,
     @Param("balance") int balance,@Param("is_ussed") int is_ussed,@Param("status") String status,
     @Param("batch_from")String batch_from,@Param("exp_date") String exp_date, @Param("product_id") String product_id, 
     @Param("stock_id") String stock_id);

     @Query(value = "SELECT batch FROM `stock_detail` WHERE product_id=?1 order BY batch DESC LIMIT 1", nativeQuery = true)
     StockDetailDTO.GetBatch findByIdProduct(String id);

    // query to select product is uploaded or not
     @Query(value = "SELECT id FROM `stock_detail` WHERE product_id=:id order by exp_date ASC LIMIT 1", nativeQuery = true)
     StockDetailDTO findByCalendar(@Param("id")String id);

     // query to check total current product
     @Query(value = "SELECT SUM(current) as current FROM `stock_detail` where product_id=:id ", nativeQuery = true)
     StockDetailDTO.GetCurrent findCountCurrent(@Param("id")String id);

     // query to select current product by exp_date and stock current > 0
     @Query(value = "SELECT id, batch, current FROM `stock_detail` WHERE product_id=:id AND current >'0' order by exp_date ASC LIMIT 1", nativeQuery = true)
     StockDetailDTO.GetCurrentProduct findProductAvailable(@Param("id")String id);

    //  query to insert to database checkout product
    @Transactional
    @Modifying
    @Query(value = "INSERT INTO 'stock_detail'"+
    "( `balance`, `batch`, `batch_from`, `current`,  `is_ussed`, `price`, `status`, `product_id`, `stock_id`)"+
     "VALUES (:balance ,:batch ,:batch_from ,:current ,:exp_date ,:is_ussed ,:price ,:status ,:product_id ,:stock_id)", nativeQuery=true)
     int createDataCheckout(@Param("batch") String batch,@Param("price") int price,@Param("current") int current,
     @Param("balance") int balance,@Param("is_ussed") int is_ussed,@Param("status") String status,
     @Param("batch_from")String batch_from, @Param("product_id") String product_id, 
     @Param("stock_id") String stock_id);

    //  query update stock product
    @Transactional
    @Modifying
    @Query(value = "UPDATE `stock_detail` "+
    "SET `current`=:current "+
     "WHERE `stock_detail`.`id`=:id ", nativeQuery=true)
     int updateDataProduct(@Param("id") int id,@Param("current") int current);

    //  query delete stock detail product
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM `stock_detail` where `id`=:id", nativeQuery=true)
     int deleteStockDetail(@Param("id") int id);
     


     @Query(nativeQuery = true,value = "SELECT `stock_detail`.id,`stock_detail`.batch,`stock_detail`.exp_date,`stock_detail`.price,"+
    "`stock_detail`.current,`stock_detail`.balance,`stock_detail`.batch_from,`stock_detail`.is_ussed,`stock_detail`.status,"+
    "`stock_detail`.product_id,`stock_detail`.stock_id, `product`.`name`"+
    " FROM `stock_detail` LEFT JOIN `product` on `stock_detail`.`product_id`=`product`.`id`"+
    " where `stock_detail`.exp_date BETWEEN ?1 AND ?2")
    List<StockDetailDTO.GetStockDetailProduct> printStockDetailSearch(@Param("dateFrom") String dateFrom,@Param("dateTo") String dateTo);
}
