package com.nd6.stok.models;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.lang.Nullable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
public class StockDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    @Column (length = 50)
    @Size(max = 50)
    @Nullable
    String batch;
    @Null
    Date expDate;
    @Nullable
    int price,current,balance,is_ussed;
    @Column (length = 50)
    @Size(max = 50)
    String status;
    @Nullable
    @Column (length = 50)
    @Size(max = 50)
    String batch_from;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "product_id")
    @JsonIgnore
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "stock_id")
    @JsonIgnore
    private Stock stock;

    public StockDetail(@Size(max = 50) String batch, Date expDate, int price, int current, int balance, int is_ussed,
            @Size(max = 50) String status, @Size(max = 50) String batch_from, Product product, Stock stock) {
        this.batch = batch;
        this.expDate = expDate;
        this.price = price;
        this.current = current;
        this.balance = balance;
        this.is_ussed = is_ussed;
        this.status = status;
        this.batch_from = batch_from;
        this.product = product;
        this.stock = stock;
    }
}
