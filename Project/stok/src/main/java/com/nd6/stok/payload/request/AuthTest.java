package com.nd6.stok.payload.request;


public class AuthTest {
    
    
    private String username;


    private String password;


    public AuthTest(String username, String password) {
        this.username = username;
        this.password = password;
    }


    public AuthTest() {
    }


    public String getUsername() {
        return username;
    }


    public void setUsername(String username) {
        this.username = username;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }
}
