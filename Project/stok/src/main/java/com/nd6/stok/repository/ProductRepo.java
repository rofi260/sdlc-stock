package com.nd6.stok.repository;

import java.util.List;

import com.nd6.stok.models.Product;
import com.nd6.stok.models.DTO.ProductDTO.ProductData;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepo extends JpaRepository<Product, String>{

    Iterable<Product> findByName(String name);
    Page<Product> findById(String id,Pageable pageable);
    List<Product> findById(String id,Sort sort);

    @Query(value = "SELECT id,name FROM `product`", nativeQuery = true)
    List<ProductData> findDataProduct();

 
    


}
