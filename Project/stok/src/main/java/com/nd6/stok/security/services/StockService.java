package com.nd6.stok.security.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nd6.stok.models.Stock;
import com.nd6.stok.payload.response.ResourceNotFoundException;
import com.nd6.stok.repository.StockRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class StockService {
    @Autowired
    StockRepository stockRepository;


    private Sort.Direction getSortDirection(String direction) {
        if (direction.equals("asc")) {
          return Sort.Direction.ASC;
        } else if (direction.equals("desc")) {
          return Sort.Direction.DESC;
        }
    
        return Sort.Direction.ASC;
      }
// get data with pagination and sorting
    public ResponseEntity<Map<String, Object>> findAll(String id,int page, int size, String[] sort) {
        try {
            List<Order> orders = new ArrayList<Order>();
      
            if (sort[0].contains(",")) {
              // sortOrder="field, direction"
              for (String sortOrder : sort) {
                String[] _sort = sortOrder.split(",");
                orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
              }
            } else {
              // sort=[field, direction]
              orders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }
      
            List<Stock> stock = new ArrayList<Stock>();
            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));
      
            Page<Stock> stockData;
            if (id == null)
              stockData = stockRepository.findAll(pagingSort);
            else
              stockData = stockRepository.findById(id, pagingSort);
      
            stock = stockData.getContent();
      
            Map<String, Object> response = new HashMap<>();
            response.put("stocks", stock);
            response.put("currentPage", stockData.getNumber());
            response.put("totalItems", stockData.getTotalElements());
            response.put("totalPages", stockData.getTotalPages());
            response.put("lastPage", stockData.isLast());
            response.put("firstPage", stockData.isFirst());
            // response.put("pagination", stockData);
      
            return new ResponseEntity<>(response, HttpStatus.OK);
          } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
          }
    }

    public ResponseEntity<List<Stock>> getAll(String id){
        List<Stock> stocks = new ArrayList<Stock>();
        if (id == null)
        stockRepository.findAll().forEach(stocks::add);
        else
        stockRepository.findById(id);
        if (stocks.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    return new ResponseEntity<>(stocks, HttpStatus.OK);
       
    }

    public ResponseEntity< Stock> save (Stock stock){  
        Stock _stock = stockRepository.save(new Stock(stock.getStock_date(),stock.getDescription(),stock.getStatus()));
        return new ResponseEntity<>(_stock, HttpStatus.CREATED);
        
    }

    public ResponseEntity<HttpStatus> delete(String id){
        stockRepository.deleteById(id);
    
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Stock> getById (String id){
        Stock stock = stockRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Not found Stock with id = " + id));
        return new ResponseEntity<>(stock, HttpStatus.OK);
       
    }
    public ResponseEntity<Stock> getCurrentStock (){
      Stock stock = stockRepository.findCurrent()
      .orElseThrow(() -> new ResourceNotFoundException("Not found Stock"));
      return new ResponseEntity<>(stock, HttpStatus.OK);
     
  }

    public ResponseEntity<Stock> updateById(String id,Stock stock){
        Stock _stock = stockRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Not found Stock with id = " + id));
    _stock.setStock_date(stock.getStock_date());
    _stock.setDescription(stock.getDescription());
    
    return new ResponseEntity<>(stockRepository.save(_stock), HttpStatus.OK);
    }

    
}
