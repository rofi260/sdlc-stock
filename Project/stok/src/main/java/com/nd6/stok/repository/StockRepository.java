package com.nd6.stok.repository;

import java.util.List;
import java.util.Optional;

import com.nd6.stok.models.Stock;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StockRepository extends JpaRepository<Stock,String> {

    Page<Stock> findById(String id, Pageable pagingSort);
    List<Stock> findById(String id,Sort sort);

    @Query(value = "SELECT id,stock_date,description,status FROM `stock`  order by id DESC LIMIT 1", nativeQuery = true)
    Optional<Stock> findCurrent();
}
