package com.nd6.stok.controllers;

import java.util.List;
import java.util.Map;



import com.nd6.stok.models.Product;
import com.nd6.stok.models.Stock;
import com.nd6.stok.models.DTO.StockDetailDTO;
import com.nd6.stok.models.DTO.ProductDTO.ProductData;
import com.nd6.stok.security.services.ProductService;
import com.nd6.stok.security.services.StockDetailService;
import com.nd6.stok.security.services.StockService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    ProductService productService;

    @Autowired
    StockService stockService;

    @Autowired
    StockDetailService stockDetailService;

    // Product 
  // @GetMapping("/product")
  // public ResponseEntity<List<Product>> getAllProduct(@RequestParam(required = false) String name) {
  //   return productService.getAll(name);
  // }

  // Product with custom pagination
  @GetMapping("/product")
  public ResponseEntity<Map<String, Object>> getAllProduct(  @RequestParam(required = false) String id,
  @RequestParam(defaultValue = "0") int page,
  @RequestParam(defaultValue = "3") int size,
  @RequestParam(defaultValue = "id,asc") String[] sort) {
    return productService.findAll(id,page,size,sort);
  }

  @GetMapping("/product/data")
  public ResponseEntity<List<ProductData>> getDataProduct(  ) {
    return productService.productData();
  }

  @GetMapping("/product/{id}")
  public ResponseEntity<Product> getProductById(@PathVariable("id") String id) {
    return productService.getById(id);
  }
  @PostMapping("/product")
  public ResponseEntity<Product> createProduct(@RequestBody Product product) {
   return productService.save(product);
  }

  @PutMapping("/product/{id}")
  public ResponseEntity<Product> updateProduct(@PathVariable("id") String id, @RequestBody Product product) {
    return productService.updateById(id, product);
  }
  @DeleteMapping("/product/{id}")
  public ResponseEntity<HttpStatus> deleteProduct(@PathVariable("id") String id) {
    return productService.delete(id);
  }


    //   Stock
    // Stock with custom pagination
    @GetMapping("/stocks")
    public ResponseEntity<Map<String, Object>> getAllStock(  @RequestParam(required = false) String id,
    @RequestParam(defaultValue = "0") int page,
    @RequestParam(defaultValue = "2") int size,
    @RequestParam(defaultValue = "id,asc") String[] sort) {
      return stockService.findAll(id,page,size,sort);
    }

    @GetMapping("/stock")
    public ResponseEntity<List<Stock>> getAllStockNative(@RequestParam(required = false) String id) {
      return stockService.getAll(id);
    }
  
    @GetMapping("/stock/{id}")
    public ResponseEntity<Stock> getStockById(@PathVariable("id") String id) {
      return stockService.getById(id);
    }
    @GetMapping("/stock/latest")
    public ResponseEntity<Stock> getCurrentStock() {
      return stockService.getCurrentStock();
    }
    @PostMapping("/stock")
    public ResponseEntity<Stock> createStock(@RequestBody Stock stock) {
     return stockService.save(stock);
    }
  
    @PutMapping("/stock/{id}")
    public ResponseEntity<Stock> updateStock(@PathVariable("id") String id, @RequestBody Stock product) {
      return stockService.updateById(id, product);
    }
    @DeleteMapping("/stock/{id}")
    public ResponseEntity<HttpStatus> deleteStock(@PathVariable("id") String id) {
      return stockService.delete(id);
    }
  

    // Stok Detail 
  @GetMapping("/stockdetail/print")
  public ResponseEntity<List<StockDetailDTO.GetStockDetailProduct>> printDetailStockProduct(
    @RequestParam(required = false, defaultValue = "")String dateFrom,
    @RequestParam(required = false, defaultValue = "")String dateTo) {
    return stockDetailService.printDetailStock(dateFrom,dateTo);
  }
    @GetMapping("/stockdetail")
    public ResponseEntity<Map<String, Object>> getDetailStock(  @RequestParam(required = false) Integer id,
    @RequestParam(defaultValue = "0") int page,
    @RequestParam(defaultValue = "10") int size,
    @RequestParam(defaultValue = "id,desc") String[] sort) {
      return stockDetailService.getAll(id,page,size,sort);
    }

    // with search
    @GetMapping("/stockdetail/cari")
    public ResponseEntity<Map<String, Object>> getDataDetailStock(  @RequestParam(required = false) Integer id,
    @RequestParam(defaultValue = "0") int page,
    @RequestParam(defaultValue = "10") int size,
    @RequestParam(defaultValue = "id,desc") String[] sort,
    @RequestParam(required = false, defaultValue = "")String search) {
      return stockDetailService.getAllSearch(id,page,size,sort,search);
    }

    @GetMapping("/stockdetail/{id}")
    public ResponseEntity<Map<String, Object>> getDetailStockById(  @PathVariable("id") String id,
    @RequestParam(defaultValue = "0") int page,
    @RequestParam(defaultValue = "5") int size,
    @RequestParam(defaultValue = "id,desc") String[] sort) {
      return stockDetailService.getByIdStock(id,page,size,sort);
    }

    @GetMapping("/stockdetail/stock/{id}")
    public StockDetailDTO.StockStock getDetailStockData(  @PathVariable("id") String id){
      return stockDetailService.getStockDetailStock(id);
      
    }

    @PostMapping("/stockdetail")
    public ResponseEntity<Object> createStockDetail(@RequestBody StockDetailDTO stockDetail) {
      System.out.println(stockDetail);
     return stockDetailService.save(stockDetail);
    }

    @PostMapping("/stockdetail/checkout")
    public ResponseEntity<Object> takeStockDetail(@RequestBody StockDetailDTO stockDetail) {
      System.out.println(stockDetail);
     return stockDetailService.takeStock(stockDetail);
    }

    @GetMapping("/stockdetail/delete")
    public ResponseEntity<Object> deleteStockById(@RequestParam int id){
      return stockDetailService.deleteStockById(id);
    }
}
