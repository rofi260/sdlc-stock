package com.nd6.stok.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nd6.stok.payload.request.AuthTest;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    
    @Test
    void testAuthenticateUser() throws Exception {
        AuthTest authTest = new AuthTest("admin","rahasia12");
        mockMvc.perform(post("/api/auth/signin")
        .contentType("application/json")
        .content(objectMapper.writeValueAsString(authTest)))
        .andExpect(status().is(500));
    }

    
}
