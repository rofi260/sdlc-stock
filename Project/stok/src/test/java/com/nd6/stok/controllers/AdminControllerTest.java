package com.nd6.stok.controllers;

import org.junit.jupiter.api.Test;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nd6.stok.models.Product;
import com.nd6.stok.models.Stock;
import com.nd6.stok.payload.request.AuthTest;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.sql.Date;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AdminControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void testCreateProduct() throws Exception{
        
    }

    @Test
    void testCreateStock() throws Exception {
        // Stock stock = new Stock("stok1",2022/05/05);
        // mockMvc.perform(get("/admin/product")
        // .contentType("application/json")
        // .content(objectMapper.writeValueAsString(stock))
        // )
        // .andExpect(status().is(200));
    }

    @Test
    void testDeleteProduct()throws Exception {
        mockMvc.perform(get("/admin/product/PROD0001")
        .contentType("application/json")
        // .content(objectMapper.writeValueAsString())
        )
        .andExpect(status().is(200));
    }

    @Test
    void testDeleteStock()throws Exception {
        mockMvc.perform(get("/admin/product/STOK0001")
        .contentType("application/json")
        // .content(objectMapper.writeValueAsString())
        )
        .andExpect(status().is(200));
    }

    @Test
    void testGetAllProduct() throws Exception {
        // Product product = new Product("admin","rahasia12");
        mockMvc.perform(get("/admin/product")
        .contentType("application/json")
        // .content(objectMapper.writeValueAsString())
        )
        .andExpect(status().is(200));
    }

    @Test
    void testGetAllStock() throws Exception {
// Product product = new Product("admin","rahasia12");
        mockMvc.perform(get("/admin/stocks")
        .contentType("application/json")
        // .content(objectMapper.writeValueAsString())
        )
        .andExpect(status().is(200));
    }

    @Test
    void getAllStockNative() throws Exception{
        mockMvc.perform(get("/admin/stock")
        .contentType("application/json")
        // .content(objectMapper.writeValueAsString())
        )
        .andExpect(status().is(200));
    }

    @Test
    void testGetDetailStock() throws Exception {
        mockMvc.perform(get("/admin/stock/STOK0001")
        .contentType("application/json")
        // .content(objectMapper.writeValueAsString())
        )
        // .andExpect(status().is(200));
        .andExpect(content().string("{\"id\":\"STOK0001\",\"stock_date\":\"2022-05-06\",\"description\":\"Stok Pertama\"}"));
    }
    

    @Test
    void testGetDetailStockById() throws Exception {
        mockMvc.perform(get("/admin/stockdetail/STOK0001")
        .contentType("application/json")
        // .content(objectMapper.writeValueAsString())
        )
        .andExpect(status().is(200));
        // .andExpect(content().string("{\"id\":\"PROD0001\",\"name\":\"popmie\",\"expDate\":\"2022-09-06\",\"quantity\":20,\"price\":1000}"));
    }

    @Test
    void testGetProductById() throws Exception {
        mockMvc.perform(get("/admin/product/PROD0001")
        .contentType("application/json")
        // .content(objectMapper.writeValueAsString())
        )
        // .andExpect(status().is(200));
        .andExpect(content().string("{\"id\":\"PROD0001\",\"name\":\"popmie\",\"expDate\":\"2022-09-06\",\"quantity\":20,\"price\":1000}"));
    }

    @Test
    void testGetStockById() throws Exception {
        mockMvc.perform(get("/admin/stock/STOK0001")
        .contentType("application/json")
        // .content(objectMapper.writeValueAsString())
        )
        // .andExpect(status().is(200));
        .andExpect(content().string("{\"id\":\"STOK0001\",\"stock_date\":\"2022-05-06\",\"description\":\"Stok Pertama\"}"));
    }

    @Test
    void testUpdateProduct() {

    }

    @Test
    void testUpdateStock() {

    }
}
